def hw3(input, output):
	fInput = open(input,'r')
	f = open(output,'w')
	stack = []
	bin = []
	for line in fInput:
		if line[0].isalpha():
			parsePrimitive(line, stack, f)
		elif line[0] == ':':
			parseBooleanOrError(line, stack)
	fInput.close()



def parsePrimitive(line, stack, f):
	if line.startswith('add'):
		doAdd(stack,bin)
	elif line.startswith('sub'):
		doSub(stack,bin)
	elif line.startswith('mul'):
		doMul(stack,bin)
	elif line.startswith('div'):
		doDiv(stack,bin)
	elif line.startswith('rem'):
		doRem(stack,bin)
	elif line.startswith('pop'):
		doPop(stack)
	elif line.startswith('push'):
		doPush(stack, line)
	elif line.startswith('swap'):
		doSwap(stack,bin)
	elif line.startswith('neg'):
		doNeg(stack,bin)
	elif line.startswith('quit'):
		doQuit(stack, f)
	elif line.startswith('an'):
		doAnd(stack,bin)
	elif line.startswith('if'):
		doIF(stack,bin)
	elif line.startswith('or'):
		doOR(stack,bin)
	elif line.startswith('not'):
		doNOt(stack,bin)
	elif line.startswith('equa'):
		doEQUAL(stack,bin)
	elif line.startswith('less'):
		doLESSTHAN(stack,bin)
	#bind
	elif line.startswith('bin'):
		doBind(stack)
	elif line.startswith('let'):
		doLET(stack,line,f)

def doLER(stack,line,f):
	stack1 = []
	if line.startswith('add'):
		doAdd(stack1,bin)
	elif line.startswith('sub'):
		doSub(stack1,bin)
	elif line.startswith('mul'):
		doMul(stack1,bin)
	elif line.startswith('div'):
		doDiv(stack1,bin)
	elif line.startswith('rem'):
		doRem(stack1,bin)
	elif line.startswith('pop'):
		doPop(stack1)
	elif line.startswith('push'):
		doPush(stack1, line)
	elif line.startswith('swap'):
		doSwap(stack1,bin)
	elif line.startswith('neg'):
		doNeg(stack1,bin)
	elif line.startswith('quit'):
		doQuit(stack1, f)
	elif line.startswith('an'):
		doAnd(stack1,bin)
	elif line.startswith('if'):
		doIF(stack1,bin)
	elif line.startswith('or'):
		doOR(stack1,bin)
	elif line.startswith('not'):
		doNOt(stack1,bin)
	elif line.startswith('equa'):
		doEQUAL(stack1,bin)
	elif line.startswith('less'):
		doLESSTHAN(stack1,bin)
	#bind
	elif line.startswith('bin'):
		doBind(stack1)
	elif line.startswith('let'):
		doLET(stack1,line,f)
		
def doAdd(stack):
	if len(stack) < 2:
		return stack.insert(0, ':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0, ':error:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			newTop = x+y
			return stack.insert(0,str(newTop))
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			newTop = x+y
			return stack.insert(0,str(newTop))
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)
			stack.pop(0)
			stack.pop(0)
			newTop = x+y
			return stack.insert(0,str(newTop))
	elif stack[0].isdigit()and stack[1].isdigit():
		x = int(stack[1])
		y = int(stack[0])
		stack.pop(0)
		stack.pop(0)
		newTop = x+y
		return stack.insert(0, str(newTop))
	else:
		return stack.insert(0,':error:')

def doSub(stack):
	if len(stack) < 2:
		return stack.insert(0, ':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0, ':error:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			newTop = y-x
			return stack.insert(0,str(newTop))
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			newTop = y-x
			return stack.insert(0,str(newTop))
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)
			stack.pop(0)
			stack.pop(0)
			newTop = y-x
			return stack.insert(0,str(newTop))
	elif stack[0].isdigit()and stack[1].isdigit():
		x = int(stack[1])
		y = int(stack[0])
		stack.pop(0)
		stack.pop(0)
		newTop = x-y
		return stack.insert(0, str(newTop))
	else:
		return stack.insert(0,':error:')


def doMul(stack):
	if len(stack) < 2:
		return stack.insert(0, ':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0, ':error:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			newTop = x*y
			return stack.insert(0,str(newTop))
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			newTop = x*y
			return stack.insert(0,str(newTop))
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)
			stack.pop(0)
			stack.pop(0)
			newTop = x*y
			return stack.insert(0,str(newTop))
	elif stack[0].isdigit()and stack[1].isdigit():
		x = int(stack[1])
		y = int(stack[0])
		stack.pop(0)
		stack.pop(0)
		newTop = x*y
		return stack.insert(0, str(newTop))
	else:
		return stack.insert(0,':error:')



def doDiv(stack):
	if len(stack) < 2:
		return stack.insert(0, ':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0, ':error:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			if x == 0:
				return stack.insert(0, ':error:')
			else:
				stack.pop(0)
				stack.pop(0)
				bin.pop(0)
				newTop = y/x
				return stack.insert(0,str(newTop))
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])
			if x == 0:
				return stack.insert(0, ':error:')
			else:
				stack.pop(0)
				stack.pop(0)
				bin.pop(0)
				newTop = y/x
				return stack.insert(0,str(newTop))
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)
			if x == 0:
				return stack.insert(0, ':error:')
			else:
				stack.pop(0)
				stack.pop(0)
				newTop = y/x
				return stack.insert(0,str(newTop))
	elif stack[0].isdigit()and stack[1].isdigit():
		x = int(stack[1])
		y = int(stack[0])
		if y == 0:
			return stack.insert(0, ':error:')
		else:
			stack.pop(0)
			stack.pop(0)
			newTop = x//y
			return stack.insert(0, str(newTop))
	else:
		return stack.insert(0,':error:')



def doRem(stack):
	if len(stack) < 2:
		return stack.insert(0, ':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0, ':error:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			if x == 0:
				return stack.insert(0, ':error:')
			else:
				stack.pop(0)
				stack.pop(0)
				bin.pop(0)
				newTop = y%x
				return stack.insert(0,str(newTop))
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])
			if x == 0:
				return stack.insert(0, ':error:')
			else:
				stack.pop(0)
				stack.pop(0)
				bin.pop(0)
				newTop = y%x
				return stack.insert(0,str(newTop))
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)
			if x == 0:
				return stack.insert(0, ':error:')
			else:
				stack.pop(0)
				stack.pop(0)
				newTop = y%x
				return stack.insert(0,str(newTop))
	elif stack[0].isdigit()and stack[1].isdigit():
		x = int(stack[1])
		y = int(stack[0])
		if y == 0:
			return stack.insert(0, ':error:')
		else:
			stack.pop(0)
			stack.pop(0)
			newTop = x % y
			return stack.insert(0, str(newTop))
	else:
		return stack.insert(0,':error:')


def doPop(stack):
	if len(stack) < 1:
		return stack.insert(0, ':error:')
	else:
		return stack.pop(0)


def doPush(stack, line):
	getList = line.split()
	if getList[1][0] == '-':
		if getList[1][1:] == '0':
			return stack.insert(0,'0')
		elif getList[1][1:].isdigit():
			return stack.insert(0, getList[1])
		else:
			return stack.insert(0, ':error:')
	elif getList[1].isdigit():
		return stack.insert(0, getList[1])
	elif getList[1][0] =='"':#read in a string 
		return stack.insert(0,getList[1])
	elif getList[1][0] ==' 'or getList[1][1:]!=' ':#read in a name
	     return stack.insert(0,getList[1])
	else:
		return stack.insert(0, ':error:')


def doSwap(stack):
	if len(stack) < 2:
		return stack.insert(0, ':error:')
	else:
		x = stack[1]
		y = stack[0]
		stack.pop(0)
		stack.pop(0)
		stack.insert(0, y)
		return stack.insert(0, x)
# And function
def doAnd(stack):
	if len(stack) < 2:
		return stack.insert(0,':error:')
	elif stack[0] ==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1]==':false:':
		x = stack[1]
		y = stack[0]
		stack.pop(0)
		stack.pop(0)
		if x == y:
			return stack.insert(0,':true:')
		else:
			return stack.insert(0,':false:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x == y:
				return stack.insert(0,':true:')
			else:
				return stack.insert(0,':false:')
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])				
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x == y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)		
			stack.pop(0)
			stack.pop(0)
			if x == y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
	else:
		return stack.insert(0,':error:')
# or function
def doOR(stack):
	if len(stack) < 2:
		return stack.insert(0,':error:')
	elif stack[0] ==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1]==':false:':
		x = stack[1]
		y = stack[0]
		stack.pop(0)
		stack.pop(0)
		if x == ':true:' or y ==':true:':
			return stack.insert(0,':true:')
		else:
			return stack.insert(0,':false:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x == ':true:'or y ==':true:':
				return stack.insert(0,':true:')
			else:
				return stack.insert(0,':false:')
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])				
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x == ':true:' or y ==':true:':
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)		
			stack.pop(0)
			stack.pop(0)
			if x == ':true:' or y ==':true:':
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
	else:
		return stack.insert(0,':error:')
	
# not function
def doNOt(stack):
	if len(stack) < 1:
		return stack.insert(0,':error:')
	elif stack[0] ==':true:'or stack[0] ==':false:':
		x = stack[0]
		stack.pop(0)
		if x == ':true:':
			return stack.insert(0,':false:')
		else:
			return stack.insert(0,':true:')
	elif stack[0]==':unit:':
		if stack[0]==':unit:':
			x = bin[':unit:']
			stack.pop(0)
			bin.pop(0)
			if x == ':true:':
				return stack.insert(0,':false:')
			else:
				return stack.insert(0,':true:')
		else:
			return stack.insert(0, ':error:')
	else:
		return stack.insert(0,':error:')
	
# equal function

def doEQUAL(stack):
	if len(stack) < 2:
		return stack.insert(0,':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0,':error:')
	elif stack[0].isdigit() and stack[1].isdigit():
		x = stack[0]
		y = stack[1]
		stack.pop(0)
		stack.pop(0)
		if x == y:
			return stack.insert(0,':true:')
		else:
			return stack.insert(0,':false:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x == y:
				return stack.insert(0,':true:')
			else:
				return stack.insert(0,':false:')
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])				
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x == y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)		
			stack.pop(0)
			stack.pop(0)
			if x == y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
	else:
		return stack.insert(0,':error:')

# Lessthan function

def doLESSTHAN(stack):
	if len(stack) < 2:
		return stack.insert(0,':error:')
	elif stack[0]==':true:'or stack[0] ==':false:'and stack[1]==':true:'or stack[1] ==':false:':
		return stack.insert(0,':error:')
	elif stack[0].isdigit() and stack[1].isdigit():
		x = stack[0]
		y = stack[1]
		stack.pop(0)
		stack.pop(0)
		if x > y:
			return stack.insert(0,':true:')
		else:
			return stack.insert(0,':false:')
	elif stack[0]==':unit:' or stack[1]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x > y:
				return stack.insert(0,':true:')
			else:
				return stack.insert(0,':false:')
		elif stack[1]==':unit:'and stack[0]!=':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])				
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if x > y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)		
			stack.pop(0)
			stack.pop(0)
			if x > y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
	else:
		return stack.insert(0,':error:')
# if function		

def doIF(stack):
	if len(stack) < 3:
		return stack.insert(0,':error:')
	elif stack[0]!=':true:'or stack[0] !=':false:'and stack[1]!=':true:'or stack[1] !=':false:' and stack[2]==':true:'or stack[2]==':false:':
		if stack[2]==':true:':
			x = stack[0]
			stack.pop(0)
			stack.pop(0)
			stack.pop(0)
			return stack.insert(0,x)
		elif stack[2]=='false':
			x = stack[1]
			stack.pop(0)
			stack.pop(0)
			stack.pop(0)
			return stack.insert(0,x)
		else:
			return stack.insert(0,':error:')
	elif stack[0]==':unit:' or stack[1]==':unit:'or stack[2]==':unit:':
		if stack[0]==':unit:'and stack[1]!=':unit:'and stack[2]!=':unit:':
			x = int(bin[':unit:'])
			y = int(stack[1])
			z = stack[2]
			stack.pop(0)
			stack.pop(0)
			bin.pop(0)
			if z =='true':
				return stack.insert(0,x)
			else:
				return stack.insert(0,':false:')
		elif stack[0]==':unit:'and stack[1]!=':unit:'and stack[2]==':unit:':
			x = int(bin[':unit:'])		
			bin.pop(0)
			y = int(stack[1])
			z = bin[':unit:']
			bin.pop(0)
			stack.pop(0)
			stack.pop(0)
			stack.pop(0)
			if z =='true':
				return stack.insert(0,x)
			else:
				return stack.insert(0,':false:')
		elif stack[1]==':unit:'and stack[0]!=':unit:'and stack[2]!=':unit:':
			x = int(stack[0])			
			y = int(bin[':unit:'])				
			bin.pop(0)
			z = stack[2]
			stack.pop(0)
			stack.pop(0)
			stack.pop(0)
			if z =='true':
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		elif stack[1]==':unit:'and stack[0]!=':unit:'and stack[2]==':unit:':
			x = int(stack[0])
			y = int(bin[':unit:'])
			bin.pop(0)
			z = bin[':unit:']
			bin.pop(0)
			stack.pop(0)
			stack.pop(0)
			stack.pop(0)
			if z =='true':
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		elif stack[1]!=':unit:'and stack[0]!=':unit:'and stack[2]==':unit:':
			x = int(stack[0])
			y = int(stack[0])
			z = bin[':unit:']
			bin.pop(0)
			stack.pop(0)
			stack.pop(0)
			stack.pop(0)
			if z =='true':
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
		else:
			x = int(bin[':unit:'])
			bin.pop(0)
			y = int(bin[':unit:'])
			bin.pop(0)		
			stack.pop(0)
			stack.pop(0)
			if x == y:
				return stack.insert(0, ':true:')
			else:
				return stack.insert(0,':false:')
	else:
		return stack.insert(0,':error:')


def doNeg(stack):
	if len(stack) < 1:
		return stack.insert(0, ':error:')
	elif stack[0]==':true:'or stack[0] ==':false:':
		return stack.insert(0, ':error:')
	elif stack[0]==':unit:':
		x = int(bin[':unit:'])
		stack.pop(0)
		bin.pop(0)
		newTop = -1*x
		return stack.insert(0,str(newTop))
	else:
		x = int(stack[0])
		stack.pop(0)
		newTop = -1*x
		return stack.insert(0, str(newTop))
##################################################
def doBind(stack):
	if len(stack) <2:
		return stack.insert(0,':error:')
	elif stack[1]!=':true:' and stack[1]!=':false:' and stack[1]!=' ':
		x = stack[0]
		bin =[':unit:']
		bin[':unit:']=x
		stack.pop(0)
		stack.pop(0)
		return stack.insert(0,bin)
	else:
		return stack.insert(0,':error:')
		

	
def doQuit(stack, f):
	for ele in stack:
		f.write(ele + '\n')
	f.close()




def parseBooleanOrError(line, stack):
	if line[1] == 'e':
		return stack.insert(0,':error:')
	elif line[1] == 't':
		return stack.insert(0,':true:')
	else:
		return stack.insert(0,':false:') 














