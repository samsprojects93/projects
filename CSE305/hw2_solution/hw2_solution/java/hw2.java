import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;



public class MyPair
{
    private final String key;
    private final String value;
    public MyPair(String aKey, String aValue)
    {
        key   = aKey;
        value = aValue;
    }

    public String key()   { return key; }
    public String value() { return value; }
}


public class hw2 {
	public static void hw2(String input, String output) throws IOException{
	
		PrintStream myconsole=new PrintStream(new File(output));
		System.setOut(myconsole);
		
		BufferedReader in = new BufferedReader(new FileReader(input));
		
		ArrayList stack = new ArrayList();
		ArrayList <MyPair> myList = new ArrayList<MyPair>();
		ArrayList letstack = new ArrayList();
		String line = in.readLine();
		while(line!=null){
			
		
			if(Character.isLetter(line.charAt(0))){
				stack = parsePrimitive(line, stack, myconsole, myList);
			}
			else if(line.charAt(0)==':'){
				stack = parseBooleanOrError(line, stack, myList);
			}				
			else{
				myconsole.println("Error command!");
			}
			
			line = in.readLine();
		
		}
		in.close();
		
		
	}
	
	public static ArrayList parseBooleanOrError(String line, ArrayList stack, ArrayList<MyPair> myList) {
		if (line.startsWith(":e")){
			stack.add(0, ":error:");
		}
		else if (line.startsWith(":t")){
			stack.add(0, ":true:");
		}
		else if (line.startsWith(":f")){
			stack.add(0, ":false:");
		}
		else if(line.startsWith("bin")){
			if(stack.size()<2){
				stack.add(0,":error:");
			}
			else if (((String)stack.get(1)).matches("[0-9]+")){
				stack.add(0,":error:");
			}
			else{
				if(stack.get(1)==":unit:"){
					String tkey = myList.get(0).key();
					String temp = myList.get(0).value();
					myList.remove(0);
					stack.remove(1);
					myList.add(new MyPair(":unit",temp));
					stack.add(0,myList);
				}
				else{
					String value = ((String)stack.get(0));
					stack.remove(0);
					stack.remove(0);
					myList.add(new MyPair(":unit:",value));
					stack.add(0,myList);
				}
			}
		}
		
		return stack;
	}
	
	public static ArrayList doMul(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size()<2){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(1));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				Integer newTop = x*y;
				stack.add(0,newTop.toString());
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(0));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				Integer newTop = x*y;
				stack.add(0,newTop.toString());
			}
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp1);
				int y = Integer.parseInt(temp2);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				myList.remove(0);
				Integer newTop = x*y;
				stack.add(0,newTop.toString());
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			int x = Integer.parseInt((String) stack.get(1));
			int y = Integer.parseInt((String) stack.get(0));
			stack.remove(0);
			stack.remove(0);
			Integer newTop = x*y;
			stack.add(0, newTop.toString());
		}
		else
			stack.add(0,":error:");
		return stack;
	}
	
	public static ArrayList doSub(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size()<2){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int y = Integer.parseInt(temp);
				int x = Integer.parseInt((String)stack.get(1));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				Integer newTop = x-y;
				stack.add(0,newTop.toString());
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(0));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				Integer newTop = x-y;
				stack.add(0,newTop.toString());
			}
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp2);
				int y = Integer.parseInt(temp1);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				myList.remove(0);
				Integer newTop = x-y;
				stack.add(0,newTop.toString());
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			int x = Integer.parseInt((String) stack.get(1));
			int y = Integer.parseInt((String) stack.get(0));
			stack.remove(0);
			stack.remove(0);
			Integer newTop = x-y;
			stack.add(0, newTop.toString());
		}
		else
			stack.add(0,":error:");
		return stack;
	}
	
	public static ArrayList doOr(ArrayList stack,ArrayList<MyPair> myList){
			if(stack.size()<2){
				stack.add(0,":error:");
			}
			else if(stack.get(0)==":true:"||stack.get(0)==":false:"||stack.get(1)==":true:"||stack.get(1)==":false:"){
				if(stack.get(0)==":true:"||stack.get(1)==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.add(0,":true:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					stack.add(0,":false:");
				}
			}
			else if(stack.get(0)==":unit:"||stack.get(1)==":unit:"||(stack.get(0)==":unit:"&&stack.get(1)==":unit:")){
				if(stack.get(0)==":unit:"&&stack.get(1)!=":unit:"){
					String temp = myList.get(0).value();
					if(temp == ":true:"&&stack.get(1)==":true:"){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":true:");
							myList.remove(0);
						}
					else if((temp == ":false:")||(stack.get(1)==":false:")){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
							myList.remove(0);
						}
					else{
							stack.add(0,":error:");
						}
					}
				else if(stack.get(1)==":unit:"&&stack.get(0)!=":unit:"){
					String temp = myList.get(1).value();
					if(temp == ":true:"&&stack.get(0)==":true:"){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":true:");
							myList.remove(0);
						}
				else if((temp == ":false:")||(stack.get(0)==":false:")){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
							myList.remove(0);
						}
						else{
							stack.add(0,":error:");
						}
					}
				else{
					String temp1 = myList.get(0).value();
					String temp2 = myList.get(1).value();
					if(temp1 ==":true:"&&temp2==":true:"){
							myList.remove(0);
							myList.remove(0);
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":true:");
						}
					else if(temp1 ==":false:"||temp2==":false:"){
							myList.remove(0);
							myList.remove(0);
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
					}
					else{
						stack.add(0,":error:");
					}
				}
			}
			else
				stack.add(0,":error:");
	}
	
	public static ArrayList doAnd(ArrayList stack, ArrayList<MyPair> myList){
			if(stack.size()<2){
				stack.add(0,":error:");
			}
			else if(stack.get(0)==":true:"||stack.get(0)==":false:"||stack.get(1)==":true:"||stack.get(1)==":false:"){
				if(stack.get(0)== ":true:"){
					stack.remove(0);
					if(stack.get(0)==":true:"){
						stack.remove(0);
						stack.add(0,":true:");
						}
					else{
						stack.remove(0);
						stack.add(0,":false:");
					}
				}
				else{
					stack.remove(0);
					stack.add(0,":false:");
				}
			}
			else if(stack.get(0)==":unit:"||stack.get(1)==":unit:"||(stack.get(0)==":unit:"&&stack.get(1)==":unit:")){
				if(stack.get(0)==":unit:"&&stack.get(1)!=":unit:"){
					String temp = myList.get(0).value();
					if(temp == ":true:"){
						if(stack.get(1)==":false:"){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
							myList.remove(0);
						}
						else if(stack.get(1)==":true:"){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":true:");
							myList.remove(0);
						}
						else
							stack.add(0,":error:");
					}
				}
				else if(stack.get(1)==":unit:"&&stack.get(0)!=":unit:"){
					String temp = myList.get(1).value();
					if(temp == ":true:"){
						if(stack.get(0)==":false:"){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
							myList.remove(0);
						}
						else if(stack.get(0)==":true:"){
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":true:");
							myList.remove(0);
						}
						else
							stack.add(0,":error:");
					}
				}
				else{
					String temp1 = myList.get(0).value();
					String temp2 = myList.get(1).value();
					if(temp1 ==":true:"){
						if(temp2 == ":true:"){
							myList.remove(0);
							myList.remove(0);
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":true:");
						}
						else{
							myList.remove(0);
							myList.remove(0);
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
						}
					}
					else if(temp1 ==":false:"){
							myList.remove(0);
							myList.remove(0);
							stack.remove(0);
							stack.remove(0);
							stack.add(0,":false:");
					}
					else{
						stack.add(0,":error:");
					}
				}
			}
			else
				stack.add(0,":error:");
	}
	
	public static ArrayList doNot(ArrayList stack,ArrayList<MyPair> myList){
		if(stack.get(0)==":true:"||stack.get(0)==":false:"){
			if(stack.get(0)==":true:"){
				stack.remove(0);
				stack.add(0,":false:");
			}
			else{
				stack.remove(0);
				stack.add(0,":true:");
			}
		}
		else if(stack.get(0)==":unit:"){
			String temp = myList.get(0).value();
			if(temp ==":true:"){
				stack.remove(0);
				myList.remove(0);
				stack.add(0,":false:");
			}
			else{
				stack.remove(0);
				myList.remove(0);
				stack.add(0,":true:");
			}
		}
		else
			stack.add(0,":error:");
	}

	public static ArrayList doAdd(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size()<2){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(1));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				Integer newTop = x+y;
				stack.add(0,newTop.toString());
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int y = Integer.parseInt(temp);
				int x = Integer.parseInt((String)stack.get(0));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				Integer newTop = x+y;
				stack.add(0,newTop.toString());
			}
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp1);
				int y = Integer.parseInt(temp2);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				myList.remove(0);
				Integer newTop = x+y;
				stack.add(0,newTop.toString());
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			int x = Integer.parseInt((String) stack.get(0));
			int y = Integer.parseInt((String) stack.get(1));
			stack.remove(0);
			stack.remove(0);
			Integer newTop = x+y;
			stack.add(0, newTop.toString());
		}
		else
			stack.add(0,":error:");
		return stack;
	}
	
	public static ArrayList doEqual(ArrayList stack, ArrayList<MyPair> myList){
		if(stack.size()<2){
			stack.add(0,":error:");
		}
		else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(1));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				if(x==y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int y = Integer.parseInt(temp);
				int x = Integer.parseInt((String)stack.get(0));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				if (x==y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}	
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp1);
				int y = Integer.parseInt(temp2);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				myList.remove(0);
				if(x==y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			if(stack.get(0)==stack.get(1)){
				stack.remove(0);
				stack.remove(0);
				stack.add(0,":true:");
			}
			else{
				stack.remove(0);
				stack.remove(0);
				stack.add(0,":false:");
			}
		}
		else
			stack.add(0,":error:");
		return stack;
	}
	
	public static ArrayList dolessThan(ArrayList stack, ArrayList<MyPair> myList){
		if(stack.size()<2){
			stack.add(0,":error:");
		}
		else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(1));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				if(x>y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int y = Integer.parseInt(temp);
				int x = Integer.parseInt((String)stack.get(0));
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				if (x>y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}	
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp1);
				int y = Integer.parseInt(temp2);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				myList.remove(0);
				if(x>y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			if(stack.get(0)!=stack.get(1)){
				int x = Integer.parseInt((String)stack.get(0));
				int y = Integer.parseInt((String)stack.get(1));
				stack.remove(0);
				stack.remove(0);
				if(x>y)
					stack.add(0,":true:");
				else
					stack.add(0,":false:");
			}
			else{
				stack.remove(0);
				stack.remove(0);
				stack.add(0,":false:");
			}
		}
		return stack;
	}
	// Bind function for string and number 
	public static ArrayList doBind(ArrayList stack,ArrayList<MyPair> myList){
		if(stack.size()<2){
			stack.add(0,":error:");
		}
		else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			String value = ((String)stack.get(0));
			stack.remove(0);
			stack.remove(0);
			myList.add(new MyPair(":unit:",value));
			stack.add(0,myList);
		}
		else
			stack.add(0,":error:");
		return stack;
	}
	//added------------------------------------------------ if function 
	public static ArrayList doIF(ArrayList stack,ArrayList<MyPair> myList){
		if(stack.size()<3){
			stack.add(0,":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(2)==":unit:")||((stack.get(0)==":unit:")&&(stack.get(1)==":unit:"))||((stack.get(0)==":unit:")&&(stack.get(1)==":unit:")&&(stack.get(2)==":unit:"))&&((stack.get(2))==":true:")||(stack.get(3)==":false:")){
			// 0 = u  1 = !u 2 = !u
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")&&(stack.get(2)!=":unit:")){
				String temp = myList.get(0).value();
				if(stack.get(2)==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
				else{
					temp = ((String)stack.get(1));
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
			}
			// 0 = u  1 = !u 2 = u
			else if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")&&(stack.get(2)==":unit:")){
				String temp = myList.get(0).value();
				String temp2 = myList.get(2).value();
				if(temp2 ==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
				else{
					temp = ((String)stack.get(1));
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
			}
			// 0 = !u  1 = u 2 = !u
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")&&(stack.get(2)!=":unit:")){
				String temp = myList.get(1).value();
				if(stack.get(2)==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
				else{
					temp = ((String)stack.get(0));
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
			}
			// 0 = !u  1 = u 2 = u
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")&&(stack.get(2)==":unit:")){
				String temp = myList.get(1).value();
				String temp2 = myList.get(2).value();
				if(temp2 ==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
				else{
					temp = ((String)stack.get(0));
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					stack.add(0,temp);
				}
			}
			// 0 = !u  1 = !u 2 = u
			else if((stack.get(1)!=":unit:")&&(stack.get(0)!=":unit:")&&(stack.get(2)==":unit:")){
				String temp3 = myList.get(2).value();
				String temp1 = ((String)stack.get(0));
				String temp2 = ((String)stack.get(1));
				if(temp2 ==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					stack.add(0,temp1);
				}
				else{
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					stack.add(0,temp2);
				}
			}						
			// 0 = u 1 = u 2 = u
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				String temp3 = myList.get(3).value();
				if(temp3 ==":true:"){
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					myList.remove(0);
					stack.add(0,temp1);
				}
				else{
					stack.remove(0);
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					myList.remove(0);
					stack.add(0,temp2);
				}
			}
		}
		else if ((((String) stack.get(0)).matches("[0-9]+"))&&(((String) stack.get(1)).matches("[0-9]+"))&&(((String)stack.get(2)).startsWith(":t"))){
			int x = Integer.parseInt((String) stack.get(0));
			Integer newTop = x;
			stack.remove(0);
			stack.remove(0);
			stack.remove(0);
			stack.add(0, newTop.toString());
		}
		else if((((String) stack.get(0)).matches("[0-9]+"))&&(((String) stack.get(1)).matches("[0-9]+"))&&(((String)stack.get(2)).startsWith(":f"))){
			int y = Integer.parseInt((String) stack.get(1));
			Integer newTop = y;
			stack.remove(0);
			stack.remove(0);
			stack.remove(0);
			stack.add(0, newTop.toString());


		}
		else
			stack.add(0,":error:");
		return stack;
	}
	

	public static ArrayList parsePrimitive(String line, ArrayList stack, PrintStream myconsole, ArrayList<MyPair> myList){
		if (line.startsWith("add")){
			stack = doAdd(stack,myList);
		}
		else if (line.startsWith("sub")){
			stack = doSub(stack,myList);
		}
		else if (line.startsWith("mul")){
			stack = doMul(stack,myList);
		}
		else if (line.startsWith("div")){
			stack = doDiv(stack,myList);
		}
		else if (line.startsWith("rem")){
			stack = doRem(stack,myList);
		}
		else if (line.startsWith("pop")){
			stack = doPop(stack,myList);
		}
		else if (line.startsWith("push")){
			stack = doPush(stack, line);
		}
		else if (line.startsWith("swap")){
			stack = doSwap(stack,myList);
		}
		else if (line.startsWith("neg")){
			stack = doNeg(stack,myList);
		}
		else if (line.startsWith("quit")){
			doQuit(stack, myconsole);
		}
		else if (line.startsWith("equ")){
			stack = doEqual(stack,myList);
		}
		else if (line.startsWith("less")){
			stack = dolessThan(stack,myList);
		}
		else if (line.startsWith("bin")){
			stack = doBind(stack,myList);
		}
		else if (line.startsWith("if")){
			stack = doIF(stack,myList);
		}
		else if (line.startsWith("an")){
			stack = doAnd(stack,myList);
		}
		else if (line.startsWith("or")){
			stack = doOr(stack,myList);
		}
		else if (line.startsWith("no")){
			stack = doNot(stack,myList);
		}
		else if (line.startsWith("let")){
			doLET(stack,myList,line,myconsole);
		}
	}

	public static void doQuit(ArrayList stack, PrintStream myconsole) {
		for (int i = 0; i < stack.size(); i++){
			myconsole.println(stack.get(i));
		}
		myconsole.close();
	}
	
	public static void doLET(ArrayList stack, ArrayList<MyPair> myList, String line,PrintStream myconsole){
		ArrayList stack1 = new ArrayList();
		if (((String)stack.get(0)).startsWith("add")){
			stack1 = doAdd(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("sub")){
			stack1 = doSub(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("mul")){
			stack1 = doMul(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("div")){
			stack1 = doDiv(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("rem")){
			stack1 = doRem(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("pop")){
			stack1 = doPop(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("push")){
			stack1 = doPush(stack1, line);
		}
		else if (((String)stack.get(0)).startsWith("swap")){
			stack1 = doSwap(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("neg")){
			stack1 = doNeg(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("quit")){
			doQuit(stack1, myconsole);
		}
		else if (((String)stack.get(0)).startsWith("equ")){
			stack1 = doEqual(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("less")){
			stack1 = dolessThan(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("bin")){
			stack1 = doBind(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("if")){
			stack1 = doIF(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("an")){
			stack1 = doAnd(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("or")){
			stack1 = doOr(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("no")){
			stack1 = doNot(stack1,myList);
		}
		else if (((String)stack.get(0)).startsWith("let")){
			doLET(stack,myList,line, myconsole);
		}	
	}

	public static ArrayList doNeg(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.isEmpty()){
			stack.add(0, ":error:");
		}
		else if ((((String) stack.get(0))== ":true:")||((String)stack.get(0)==":false:")){
			stack.add(0, ":error:");
		}
		else if (((String)stack.get(0))==":unit"){
			String temp = myList.get(0).value();
			int x = Integer.parseInt(temp);
			Integer newTop = -1*x;
			myList.remove(0);
			stack.remove(0);
			stack.add(0,newTop.toString());
		}
		else if(((String)stack.get(0)).matches("[0-9]+")){
			int x = Integer.parseInt((String) stack.get(0));
			Integer newTop = -1*x;
			stack.remove(0);
			stack.add(0, newTop.toString());
			}
		else
			stack.add(0,":error:");
		return stack;
	}

	private static ArrayList doSwap(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size() < 2){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((((String)stack.get(0))==":unit:")&&(((String)stack.get(1))!=":unit:")){
				String temp1 = myList.get(0).value();
				String temp2 = (String)stack.get(1);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				stack.add(0,temp2);
				stack.add(0,temp1);
			}
			else if((((String)stack.get(1))==":unit:")&&(((String)stack.get(0))!=":unit:")){
				String temp1 = myList.get(0).value();
				String temp2 = (String)stack.get(0);
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				stack.add(0,temp1);
				stack.add(0,temp2);
			}
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				stack.remove(0);
				stack.remove(0);
				myList.remove(0);
				myList.remove(0);
				stack.add(0,temp1);
				stack.add(0,temp2);
			}
		}
		else{
			String x = (String) stack.get(1);
			String y = (String) stack.get(0);
			stack.remove(0);
			stack.remove(0);
			stack.add(0, y);
			stack.add(0, x);
		}
		return stack;
	}
	

	public static ArrayList doPush(ArrayList stack, String line) {
//		String[] lineList = line.split(" ");
//		String getNum = Arrays.asList(lineList).get(1);
//		stack.add(0, getNum);
		
		//stack.add(0, line.substring(5));
		String getNum = line.substring(5);
		if (getNum.charAt(0) == '-'){
			if (getNum.substring(1).equals("0")){
				stack.add("0");
			}
			else if (getNum.substring(1).matches("[0-9]+")){
				stack.add(0, getNum);
			}
			else{
				stack.add(0, ":error:");
			}
		}
		else if (getNum.matches("[0-9]+")){
			stack.add(0, getNum);
		}
		//push a string 
		else if ((getNum.charAt(0)=='"')&&(getNum.charAt(getNum.length()-1)=='"')){
			stack.add(0,getNum.substring(1,getNum.length()-1));
		}
		//push name     need more work !!!!!
		else if ((getNum.charAt(0) ==' ')&&(getNum.charAt(1)!=' ')){
			String NameOf = getNum.substring(1);
			stack.add(0,NameOf);
		}
		else{
			stack.add(0, ":error:");
		}
		return stack;
	}

	public static ArrayList doPop(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size() < 1){
			stack.add(0, ":error:");
		}
		else{
			stack.remove(0);
		}
		return stack;
	}

	public static ArrayList doRem(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size()<2){
			stack.add(0, ":error:");
		}
	    else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(1));
				if (y == 0){
					stack.add(0, ":error:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					Integer newTop = x%y;
					stack.add(0, newTop.toString());
				}
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(0));
				if (y == 0){
					stack.add(0, ":error:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					Integer newTop = x%y;
					stack.add(0, newTop.toString());
				}
			}
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp2);
				int y = Integer.parseInt(temp1);
				if (y == 0){
					stack.add(0, ":error:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					Integer newTop = x%y;
					stack.add(0, newTop.toString());
				}
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			int x = Integer.parseInt((String) stack.get(1));
			int y = Integer.parseInt((String) stack.get(0));
			if (y == 0){
				stack.add(0, ":error:");
			}
			else{
				stack.remove(0);
				stack.remove(0);
				Integer newTop = x%y;
				stack.add(0, newTop.toString());
			}
		}
		else
			stack.add(0,":error:");
		return stack;
	}

	public static ArrayList doDiv(ArrayList stack, ArrayList<MyPair> myList) {
		if (stack.size()<2){
			stack.add(0, ":error:");
		}
	    else if ((stack.get(0)== ":t" )||(stack.get(1)== ":t")|| (stack.get(1)== ":f")||(stack.get(0)== ":f")){
			stack.add(0, ":error:");
		}
		else if ((stack.get(0)==":unit:")||(stack.get(1)==":unit:")||(stack.get(0)==":unit:"&&(stack.get(1)==":unit:"))){
			if((stack.get(0)==":unit:")&&(stack.get(1)!=":unit:")){
				String temp = myList.get(0).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(1));
				if (y == 0){
					stack.add(0, ":error:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					Integer newTop = x/y;
					stack.add(0, newTop.toString());
				}
			}
			else if((stack.get(1)==":unit:")&&(stack.get(0)!=":unit:")){
				String temp = myList.get(1).value();
				int x = Integer.parseInt(temp);
				int y = Integer.parseInt((String)stack.get(0));
				if (y == 0){
					stack.add(0, ":error:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					Integer newTop = x/y;
					stack.add(0, newTop.toString());
				}
			}
			else{
				String temp1 = myList.get(0).value();
				String temp2 = myList.get(1).value();
				int x = Integer.parseInt(temp2);
				int y = Integer.parseInt(temp1);
				if (y == 0){
					stack.add(0, ":error:");
				}
				else{
					stack.remove(0);
					stack.remove(0);
					myList.remove(0);
					myList.remove(0);
					Integer newTop = x/y;
					stack.add(0, newTop.toString());
				}
			}
		}
		else if ((((String)stack.get(0)).matches("[0-9]+"))&&(((String)stack.get(1)).matches("[0-9]+"))){
			int x = Integer.parseInt((String) stack.get(1));
			int y = Integer.parseInt((String) stack.get(0));
			if (y == 0){
				stack.add(0, ":error:");
			}
			else{
				stack.remove(0);
				stack.remove(0);
				Integer newTop = x/y;
				stack.add(0, newTop.toString());
			}
		}
		else
			stack.add(0,":error:");
		return stack;
	}
}
