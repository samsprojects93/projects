def hw2(inpt, output):
    outptfile = open(output, 'w')
    readfile = open(inpt, 'r').readlines()
    stack = []
    open = True
    for line in readfile:
        inline = line.split()
        open = True
        if (len(inline)) > 1:
            if inline[0] == "push":
                hold = inline.pop()
                if float(hold) % 1 != 0:
                    stack.append("Error")
                    open = False
                else:
                    inline.append(hold)
                    
                if open:          
                    if not(inline[1].isdigit()) and not(str((int(inline[1]) * (-1))).isdigit()):
                        stack.append("Error")
                    else:
                        stack.append(inline[1]) 
        else:
            if inline[0] == "quit":
                stack.reverse()
                for i in stack:
                    outptfile.write(str(i) + "\n")
            else if inline[0] == "pop":
                if len(stack) < 1:
                    stack.append("Error")
                else:
                    stack.pop()
            else if inline[0] == "Error":
                stack.append("Error")
            else if inline[0] == "True":
                stack.append("True")
            else if inline[0] == "False":
                stack.append("False")
            else if inline[0] == "neg":
                if len(stack) < 1:
                    stack.append("Error")
                else if stack[len(stack)-1] == "Error" or stack[len(stack)-1] == "True" or stack[len(stack)-1] == "False":
                    stack.append("Error")
                else if not(str(stack[len(stack)-1]).isdigit()) and not(str(int(stack[len(stack)-1]) * (-1)).isdigit()):
                    stack.append("Error")
                else:
                    temp = stack.pop()
                    temp = int(temp) * -1
                    stack.append(temp)
            else if inline[0] == "sub":
                if len(stack) < 2:
                    stack.append("Error")
                else if stack[len(stack)-1] == "Error" or stack[len(stack)-1] == "True" or stack[len(stack)-1] == "False" or stack[len(stack)-2] == "Error" or stack[len(stack)-2] == "True" or stack[len(stack)-2] == "False":
                    stack.append("Error")
                else if (not(str(stack[len(stack)-1]).isdigit()) and not(str(int(stack[len(stack)-1]) * (-1)).isdigit())) or (not(str(stack[len(stack)-2]).isdigit()) and not(str(int(stack[len(stack)-2]) * (-1)).isdigit())):
                    stack.append("Error")
                else:
                    a = stack.pop() 
                    b = stack.pop() 
                    temp = int(y) - int(x)
                    stack.append(temp)					
            else if inline[0] == "add":
                if len(stack) < 2:
                    stack.append("Error")
                else if stack[len(stack)-1] == "Error" or stack[len(stack)-1] == "True" or stack[len(stack)-1] == "False" or stack[len(stack)-2] == "Error" or stack[len(stack)-2] == "True" or stack[len(stack)-2] == "False":
                    stack.append("Error")
                else if (not(str(stack[len(stack)-1]).isdigit()) and not(str(int(stack[len(stack)-1]) * (-1)).isdigit())) or (not(str(stack[len(stack)-2]).isdigit()) and not(str(int(stack[len(stack)-2]) * (-1)).isdigit())):
                    stack.append("Error")
                else:
                    a = stack.pop() 
                    b = stack.pop() 
                    temp = int(x) + int(y)
                    stack.append(temp)
            else if inline[0] == "mul":
                if len(stack) < 2:
                    stack.append("Error")
                else if stack[len(stack)-1] == "Error" or stack[len(stack)-1] == "True" or stack[len(stack)-1] == "False" or stack[len(stack)-2] == "Error" or stack[len(stack)-2] == "True" or stack[len(stack)-2] == "False":
                    stack.append("Error")
                else if (not(str(stack[len(stack)-1]).isdigit()) and not(str(int(stack[len(stack)-1]) * (-1)).isdigit())) or (not(str(stack[len(stack)-2]).isdigit()) and not(str(int(stack[len(stack)-2]) * (-1)).isdigit())):
                    stack.append("Error")
                else:
                    a = stack.pop()
                    b = stack.pop()
                    temp = int(x) * int(y)
                    stack.append(temp)
            else if inline[0] == "div":
                if len(stack) < 2:
                    stack.append("Error")
                else if stack[len(stack)-1] == "Error" or stack[len(stack)-1] == "True" or stack[len(stack)-1] == "False" or stack[len(stack)-2] == "Error" or stack[len(stack)-2] == "True" or stack[len(stack)-2] == "False":
                    stack.append("Error")
                else if (not(str(stack[len(stack)-1]).isdigit()) and not(str(int(stack[len(stack)-1]) * (-1)).isdigit())) or (not(str(stack[len(stack)-2]).isdigit()) and not(str(int(stack[len(stack)-2]) * (-1)).isdigit())):
                    stack.append("Error")
                else:
                    a = stack.pop()
                    b = stack.pop()
                    temp = int(y)/int(x)
                    stack.append(int(temp))
            else if inline[0] == "rem":
                if len(stack) < 2:
                    stack.append("Error")
                else if stack[len(stack)-1] == 0:
                    stack.append("Error")
                else if stack[len(stack)-1] == "Error" or stack[len(stack)-1] == "True" or stack[len(stack)-1] == "False" or stack[len(stack)-2] == "Error" or stack[len(stack)-2] == "True" or stack[len(stack)-2] == "False":
                    stack.append("Error")
                else if (not(str(stack[len(stack)-1]).isdigit()) and not(str(int(stack[len(stack)-1]) * (-1)).isdigit())) or (not(str(stack[len(stack)-2]).isdigit()) and not(str(int(stack[len(stack)-2]) * (-1)).isdigit())):
                    stack.append("Error")
                else:
                    a = stack.pop()
                    b = stack.pop()
                    temp = int(y)%int(x) 
                    stack.append(int(temp))    			
            else if inline[0] == "swap":
                if len(stack) < 1:
                       stack.append("Error")
                else if len(stack) < 2:
                    stack.append("Error")
                else:
                    a = stack.pop()
                    b = stack.pop()
                    stack.append(x)
                    stack.append(y)

hw2('input_file.txt','output_file.txt')