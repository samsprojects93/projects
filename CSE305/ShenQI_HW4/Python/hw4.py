import re
import copy
fInput = 0

def hw4(input, output):
    global fInput
    fInput = open(input,'r')
    f = open(output,'w')
    stack = []
    hm = {}
    for line in fileRead(fInput):
        if line.startswith('let'):
            stack.insert(0,parseLet(fInput,hm,f))
        elif line.startswith('fun') or line.startswith('inOutFun'):
            parseFun(line, stack, hm, f)
        elif line.startswith('call'):
            hm_save = copy.deepcopy(hm)
            ret = prepareFuncCall(stack, hm, f)
            hm = copy.deepcopy(hm_save)
            # If we receive dict as result (called inOutFunc)
            if type(ret) == type({}):
                hm[ret['arg']] = ret['val']
            else: 
                stack.insert(0, ret)
        elif line[0].isalpha():
            parsePrimitive(line, stack, hm, f)
        elif line[0] == ':':
            parseBooleanOrError(line, stack, hm)
		
    fInput.close()

def fileRead(input):
    for line in input:
        yield line 
		
def prepareFuncCall(stack, hm, f):
    # Check if we have at least 2 args on the stack
    if len(stack) < 2:
        return ':error:'
    # Retrive function name and argument
    name = stack[0]
    arg = stack[1]
    # If we have bad function name or argument or function with that name does not exists return error
    if name == ':error:' or arg == ':error:' or name not in hm:
        return ':error:'
    # Lookup to dictionary for function data
    fun = hm.get(name)
    # Function form dictionary should be aswell dictionary so lets check the type
    if type(fun) != type({}):
        return ':error:'
    # Remove call arguments from stack
    stack.pop(0)
    stack.pop(0)
    # Check function type
    local_hm = fun['closure']
    # Copy to hm only keys that not exist in closure already
    for k in hm:
        if k not in local_hm and k != fun['param']:
            local_hm[k] = hm[k]
    local_stack = []

    if re.match('^[a-zA-Z].*', arg, 0):
        if arg not in hm:
            return ':error:'
        var = hm.get(arg)
        local_hm[fun['param']] = var
    elif arg.isdigit() or arg == ':true:' or arg == ':false:':
        local_hm[fun['param']] = arg

    if fun['type'] == 'inOutFun':
        ret, returned = runInOutFunc(fun, local_stack, local_hm, f)
        if returned == True:
            stack.insert(0, ret)
        return {'arg' : arg, 'val' : ret}
    else:
        ret = runFunc(fun, local_stack, local_hm, f)
        return ret
	
def runFunc(fun, stack, hm, f):
    generator = fileRead(fun["body"])
    for line in generator:
        if line == 'return':
            break
        elif line.startswith('let'):
            stack.insert(0, parseLet(generator, hm, f))
        elif line.startswith('fun') or line.startswith('inOutFun'):
            parseFun(line, stack, hm, f)
        elif line.startswith('call'):
            hm_save = copy.deepcopy(hm)
            ret = prepareFuncCall(stack, hm, f)
            hm = copy.deepcopy(hm_save)
            # If we receive dict as result (called inOutFunc)
            if type(ret) == type({}):
                hm[ret['arg']] = ret['val']
            else: 
                stack.insert(0, ret)
        elif line[0].isalpha():
            parsePrimitive(line, stack, hm, f)
        elif line[0] == ':':
            parseBooleanOrError(line, stack, hm)

    last_frame = stack[0]
    if re.match('^[a-zA-Z].*', last_frame, 0) and last_frame in hm and type(hm.get(last_frame)) != type({}) :
        return hm.get(last_frame)
    else:
        return last_frame
		
def runInOutFunc(fun, stack, hm, f):
    generator = fileRead(fun["body"])
    ret_reached = False
    for line in generator:
        if line == 'return':
            ret_reached = True
            break
        elif line.startswith('let'):
            stack.insert(0, parseLet(gen, hm, f))
        elif line.startswith('fun') or line.startswith('inOutFun'):
            parseFun(line, stack, hm, f)
        elif line.startswith('call'):
            hm_save = copy.deepcopy(hm)
            ret = prepareFuncCall(stack, hm, f)
            hm = copy.deepcopy(hm_save)
            # If we receive dict as result (called inOutFunc)
            if type(ret) == type({}):
                hm[ret['arg']] = ret['val']
            else: 
                stack.insert(0, ret)
        elif line[0].isalpha():
            parsePrimitive(line, stack, hm, f)
        elif line[0] == ':':
            parseBooleanOrError(line, stack, hm)

    return hm.get(fun['param']), ret_reached

def parseFunc(declar, stack, hm, f):
    global fInput
    (type, name, param) = declar.split()
    body = []
    for line in fileRead(fInput):
        if line.startswith('funEnd'):
            hm[name] = {'type'  :  type, 'param' : param, 'body' : body, 'closure' : copy.deepcopy(hm)}
            return stack.insert(0, ':unit:')
        else:
            body.append(line.strip());


def parseLet(input,hm_parent,f):
    stack_let = []
    hm_let = copy.deepcopy(hm_parent)
    for line in fileRead(input):
        if line.startswith('end'):
            top  = stack_let[0]
            if re.match('^[a-zA-Z].*', top, 0):
                top = str(hm_let.get(top))
                if top == None:
                    return ':error:'
            if top.isdigit:
                return top
            else:
                return ':error:'
        elif line.startswith('let'):
            stack_let.insert(0,parseLet(input, hm_let, f))
        elif line.startswith('fun') or line.startswith('inOutFun'):
            parseFun(line, stack_let, hm_let, f)
        elif line.startswith('call'):
            hm_save = copy.deepcopy(hm_let)
            ret = prepareFuncCall(stack_let, hm_let, f)
            hm_let = copy.deepcopy(hm_save)
            # If we receive dict as result (called inOutFunc)
            if type(ret) == type({}):
                hm_let[ret['arg']] = ret['val']
            else: 
                stack_let.insert(0, ret)
        elif line[0].isalpha():
            parsePrimitive(line, stack_let, hm_let, f)
        elif line[0] == ':':
            parseBooleanOrError(line, stack_let, hm_let)

def parsePrimitive(line, stack, hm, f):
    if line.startswith('add'):
        doAdd(stack, hm)
    elif line.startswith('sub'):
        doSub(stack, hm)
    elif line.startswith('mul'):
        doMul(stack, hm)
    elif line.startswith('div'):
        doDiv(stack, hm)
    elif line.startswith('rem'):
        doRem(stack, hm)
    elif line.startswith('pop'):
        doPop(stack)
    elif line.startswith('push'):
        doPush(stack, line)
    elif line.startswith('swap'):
        doSwap(stack)
    elif line.startswith('neg'):
        doNeg(stack, hm)
    elif line.startswith('quit'):
        doQuit(stack, f)
    elif line.startswith('if'):
        doIf(stack, hm)
    elif line.startswith('not'):
        doNot(stack, hm)
    elif line.startswith('and'):
        doAnd(stack, hm)
    elif line.startswith('or'):
        doOr(stack, hm)
    elif line.startswith('equal'):
        doEqual(stack, hm)
    elif line.startswith('lessThan'):
        doLessThan(stack, hm)
    elif line.startswith('bind'):
        doBind(stack, hm)

def doAdd(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            stack.pop(0)
            stack.pop(0)
            newTop = x+y
            return stack.insert(0, str(newTop))
        else:
            return stack.insert(0, ':error:')


def doSub(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            stack.pop(0)
            stack.pop(0)
            newTop = x-y
            return stack.insert(0, str(newTop))
        else:
            return stack.insert(0, ':error:')



def doMul(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            stack.pop(0)
            stack.pop(0)
            newTop = x*y
            return stack.insert(0, str(newTop))
        else:
            return stack.insert(0, ':error:')



def doDiv(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            if y == 0:
                return stack.insert(0, ':error:')
            stack.pop(0)
            stack.pop(0)
            newTop = x / y
            return stack.insert(0, str(newTop))
        else:
            return stack.insert(0, ':error:')



def doRem(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            if y == 0:
                return stack.insert(0, ':error:')
            stack.pop(0)
            stack.pop(0)
            newTop = x%y
            return stack.insert(0, str(newTop))
        else:
            return stack.insert(0, ':error:')


def doPop(stack):
    if len(stack) < 1:
        return stack.insert(0, ':error:')
    else:
        return stack.pop(0)


def doPush(stack, line):
    getList = line.split(' ',1)
    getList[1] = getList[1].strip('\n')
    if getList[1][0] == '-':
        if getList[1][1:] == '0':
            return stack.insert(0,'0')
        elif getList[1][1:].isdigit():
            return stack.insert(0, getList[1])
        else:
            return stack.insert(0, ':error:')
    elif getList[1].isdigit():
        return stack.insert(0, getList[1])
    elif re.match('^[a-zA-Z].*',getList[1],0):
        return stack.insert(0, getList[1])
    elif re.match('^".+"$',getList[1],0):
        return stack.insert(0, getList[1])
    else:
        return stack.insert(0, ':error:')


def doSwap(stack):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    else:
        x = stack[1]
        y = stack[0]
        stack.pop(0)
        stack.pop(0)
        stack.insert(0, y)
        return stack.insert(0, x)



def doNeg(stack, hm):
    if len(stack) < 1:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if s0.isdigit:
            x = int(s0)
            stack.pop(0)
            newTop = -1*x
            return stack.insert(0, str(newTop))
        else:
            return stack.insert(0, ':error:')


def doQuit(stack, f):
    for ele in stack:
        f.write(ele.strip('"') + '\n')
    f.close()

def parseBooleanOrError(line, stack, hm):
    if line[1] == 'e':
        return stack.insert(0,':error:')
    elif line[1] == 't':
        return stack.insert(0,':true:')
    else:
        return stack.insert(0,':false:')

def doIf(stack, hm):
    if len(stack) < 3:
        return stack.insert(0, ':error:')
    else:
        s2 = str(stack[2])
        if s2 != ':true:' and s2 != ':false:':
            s2 = str(hm.get(s2,s2))
        if s2 == ':true:':
            stack.pop(2)
            stack.pop(1)
            return stack
        elif s2 == ':false:':
            stack.pop(2)
            stack.pop(0)
				
            return stack
        else:
            return stack.insert(0,':error:')

def doNot(stack, hm):
    if len(stack) < 1:
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        if s0 != ':true:' and s0 != ':false:':
            s0 = str(hm.get(s0,s0))
        if s0 == ':true:':
            stack.pop(0)
            stack.insert(0,':false:')
            return stack
        elif s0 == ':false:':
            stack.pop(0)
            stack.insert(0,':true:')
            return stack
        else:
            return stack.insert(0,':error:')
        
def doAnd(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if s0 != ':true:' and s0 != ':false:':
            s0 = str(hm.get(s0,s0))
        if s1 != ':true:' and s1 != ':false:':
            s1 = str(hm.get(s1,s1))
        if s0 == ':false:' and s1 == ':false:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':false:')
            return stack
        elif s0 == ':false:' and s1 == ':true:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':false:')
            return stack
        elif s0 == ':true:' and s1 == ':false:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':false:')
            return stack
        elif s0 == ':true:' and s1 == ':true:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':true:')
            return stack
        else:
            return stack.insert(0,':error:')
        
def doOr(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if s0 != ':true:' and s0 != ':false:':
            s0 = str(hm.get(s0,s0))
        if s1 != ':true:' and s1 != ':false:':
            s1 = str(hm.get(s1,s1))
        if s0 == ':false:' and s1 == ':false:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':false:')
            return stack
        elif s0 == ':false:' and s1 == ':true:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':true:')
            return stack
        elif s0 == ':true:' and s1 == ':false:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':true:')
            return stack
        elif s0 == ':true:' and s1 == ':true:':
            stack.pop(0)
            stack.pop(0)
            stack.insert(0,':true:')
            return stack
        else:
            return stack.insert(0,':error:')
        
def doEqual(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            stack.pop(0)
            stack.pop(0)
            if x==y:
                return stack.insert(0, ':true:')
            else:
                return stack.insert(0, ':false:')
        else:
            return stack.insert(0, ':error:')
        
def doLessThan(stack, hm):
    if len(stack) < 2:
        return stack.insert(0, ':error:')
    elif stack[0][0] == ':' or stack[1][0] == ':':
        return stack.insert(0, ':error:')
    else:
        s0 = str(stack[0])
        s1 = str(stack[1])
        if re.match('^[a-zA-Z].*',s0,0):
            s0 = str(hm.get(s0,s0))
        if re.match('^[a-zA-Z].*',s1,0):
            s1 = str(hm.get(s1,s1))
        if s0.isdigit and s1.isdigit:
            y = int(s0)
            x = int(s1)
            stack.pop(0)
            stack.pop(0)
            if x<y:
                return stack.insert(0, ':true:')
            else:
                return stack.insert(0, ':false:')
        else:
            return stack.insert(0, ':error:')
        
def doBind(stack, hm):
    if len(stack) < 2:
        return stack.insert(0,':error:')
    else:
        s0 = stack[0]
        s1 = stack[1]
        if re.match('^[a-zA-Z].*',stack[1],0) and stack[0] != ':error:':
            stack.pop(0)
            stack.pop(0)
            s0 = str(hm.get(s0,s0))
            hm[s1] = s0
            return stack.insert(0,':unit:')
        else:
            return stack.insert(0,':error:')
