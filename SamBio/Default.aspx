﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    	<section id="one">
	        <!-- One -->
						<header class="major">
							<h2>Welcome to my website !</h2>
						</header>
						<p>My attached resume outlines all that I could offer your organization. Here's the overview:
My educational background has prepared me for the role of becoming a member of organization.  In particular, my study of Computer Science has given me a solid background so that I can perform basic programming. I am eager to contribute my enthusiasm and up-to-date skills to your organization.
I am certain that my resume will give you a greater understanding of my qualifications for this exciting opportunity.
I would greatly appreciate the opportunity to work with and learn from you. I look forward to speaking with you soon.
</p>
						<ul class="actions">
							<li><a href="Default2.aspx" class="button">Learn More</a></li>
						</ul>
					</section>

				<!-- Two -->
					<section id="two">
						<h2>Recent Work</h2>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<a href="images/fulls/Capture.jpg" class="image fit thumb"><img src="images/thumbs/Capture.jpg" alt="" /></a>
								<h3>Greenopia</h3>
								<p>Full Stack Developer (Intern)	</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="images/fulls/HJKI.jpg" class="image fit thumb"><img src="images/thumbs/HJKI.jpg" alt="" /></a>
								<h3>Hunter James Kelly Research Institute </h3>
								<p>Full Stack Developer (Intern)	</p>
							</article>
							<article class="6u 12u$(xsmall) work-item">
								<a href="images/fulls/ThetaTau.jpg" class="image fit thumb"><img src="images/thumbs/ThetaTau.jpg" alt="" /></a>
								<h3>Theta Tau </h3>
								<p>Theta Tau (ΘΤ) Website Committee</p>
							</article>
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="images/fulls/ThetaTau.jpg" class="image fit thumb"><img src="images/thumbs/ThetaTau.jpg" alt="" /></a>
								<h3>Theta Tau </h3>
								<p>Theta Tau (ΘΤ) Room Committee</p>
							</article>
						</div>
						<!--<ul class="actions">
							<li><a href="#" class="button">Full Portfolio</a></li>
						</ul>-->
					</section>

				<!-- Three -->
				<!--	<section id="three">
						<h2>Get In Touch</h2>
						<div class="row">
							<div class="8u 12u$(small)">
								<form method="post" action="#">
									<div class="row uniform 50%">
										<div class="6u 12u$(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
										<div class="6u$ 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
										<div class="12u$"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
									</div>
								</form>
								<ul class="actions">
									<li><input type="submit" value="Send Message" /></li>
								</ul>
							</div>
							<div class="4u$ 12u$(small)">
								<ul class="labeled-icons">
									<li>
										<h3 class="icon fa-home"><span class="label">Address</span></h3>
										11204 60th street.<br />
										Brooklyn, New York<br />
										United States
									</li>
									<li>
										<h3 class="icon fa-mobile"><span class="label">Phone</span></h3>
										347-757-7730
									</li>
									<li>
										<h3 class="icon fa-envelope-o"><span class="label">Email</span></h3>
										<a href="#">qisamsam@gmail.com</a>
									</li>
								</ul>
							</div>
						</div>
					</section>-->
</asp:Content>

