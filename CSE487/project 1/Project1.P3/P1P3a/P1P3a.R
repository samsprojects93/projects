library(gdata)
mtn <- read.csv("rollingsales_manhattan.csv",header=TRUE)
head(mtn)
summary(mtn)
mtn$SALE.PRICE.N <- as.numeric(gsub("[^[:digit:]]","",mtn[ ,'SALE.PRICE']))
mtn$GROSS.SQUARE.FEET <- as.numeric(gsub("[^[:digit:]]","",mtn[,'GROSS.SQUARE.FEET']))
mtn$LAND.SQUARE.FEET <- as.numeric(gsub("[^[:digit:]]","",mtn[,'LAND.SQUARE.FEET']))
#mtn$sale.date <- as.Date(mtn$SALE.DATE)  DOESNT WORK !!!!!!!
mtn$YEAR.BUILT <- as.numeric(as.character(mtn[,'YEAR.BUILT']))
attach(mtn)
hist(SALE.PRICE.N)
hist(SALE.PRICE.N[SALE.PRICE.N>0])
hist(GROSS.SQUARE.FEET[SALE.PRICE.N==0])




detach(mtn)
mtn.sale <- mtn[mtn$SALE.PRICE.N!=0,]
plot(mtn.sale$GROSS.SQUARE.FEET,mtn.sale$SALE.PRICE.N)
plot(log(mtn.sale$GROSS.SQUARE.FEET),log(mtn.sale$SALE.PRICE.N))
mtn.homes <- mtn.sale[which(grepl("FAMILY",mtn.sale$BUILDING.CLASS.CATEGORY)),]
plot(log(mtn.homes$GROSS.SQUARE.FEET),log(mtn.homes$SALE.PRICE.N))
mtn.homes[which(mtn.homes$SALE.PRICE.N<100000),][order(mtn.homes[which(mtn.homes$SALE.PRICE.N<100000),]$SALE.PRICE.N),]
mtn.homes$outliers <- (log(mtn.homes$SALE.PRICE.N) <=5) + 0
mtn.homes <- mtn.homes[which(mtn.homes$outliers==0),]
plot(log(mtn.homes$GROSS.SQUARE.FEET),log(mtn.homes$SALE.PRICE.N))

