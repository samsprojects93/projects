import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCount {

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      /*StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
        word.set(itr.nextToken());
        context.write(word, one);
------------------------------------------------------------------------		
	
		String [] filearry = value.toString().split(",");
		String half = filearry [2];
		if(filearry[1].equals("Unknown")||filearry[7].equals("")||!StringUtils.isNumeric(filearry[7]))return;
		String [] temp= filearry[2].split(" ");
		String newkey = temp[0]+"_"+filearry[1];
		Text word = new Text();
		word.set(newkey);
		context.write(word,new IntWritable(filearry[8]));*/
		
	/* Q1 What is the largest capacity room on campus amongst all the years? */
		String [] filearry = value.toString().split(",");   // split the String by  ","  then put into a array of string 
		if(filearry[1].equals("Unknown")||filearry[7].equals("")||!StringUtils.isNumeric(filearry[7]))return;
		word.set(filearry[1]);
		context.write(word,new IntWritable(Integer.parseInt(filearry[8])));
	//   this will give me all the years and capacity -->  filearry[1],filearry[8]
	
	/* Q2  How many Writing 1 are offered throughout the all year?  */
		String [] filearry = value.toString().split(",");
		if(filearry[1].equals("Unknown")||filearry[7].equals("")||!StringUtils.isNumeric(filearry[7]))return;
		if(filearry[5]!="007967")return;
		word.set(filearry[5]);
		context.write(word.new IntWritable(1));
	  }
	 /* Q3 How many Philosophy Topic  are offered throughout the all year?   */
		String [] filearry = value.toString().split(",");
		if(filearry[1].equals("Unknown")||filearry[7].equals("")||!StringUtils.isNumeric(filearry[7]))return;
		if(filearry[5]!="105238")return;   // <------ the only thing you have to change 
		word.set(filearry[5]);
		context.write(word.new IntWritable(1));
	  }
  }

  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
    /*  int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);   */
	  
//---------------------------------------------------
	/*  Q1 
		int v1 =0;
		int v2 =0;
		int temp =0;
		Iterator<IntWritable> iterator = values.iterator();
		while(iterator.hasNext()){
			v1 = iterator.next().get();
			if(!iterator.hasNext())return;
			v2 = iterator.next().get();
			if(v1 > v2)
				temp = v1;
			else
				temp = v2;
		}
		context.write(key,new IntWritable(temp))
	*/
	/*  Q2   Q3 -------------  Q20
		int temp =0;
		for (IntWritable val : values){
			temp += val.get();
		}
		context.write(key,new IntWritable(temp))
    }
	*/
	
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(WordCount.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
