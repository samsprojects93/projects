package code;

import java.util.HashMap;

import util.general.CharacterFromFileReader;

public class WriteUp {
public HashMap<String,Integer> solution(String s){
	HashMap<String,Integer> ret = new HashMap<String,Integer>();
	CharacterFromFileReader read = new CharacterFromFileReader (s);
	String b ="";
	while (read.hasNext()){
		char a = read.next();
		if(a=='\n'||a=='('||a==')'||a==','||a=='.'||a==' '){
			if(!b.equals("")){
				if(ret.containsKey(b)){
					 ret.put(b, ret.get(b)+1);
				}
				else
					ret.put(b, 1);
			}
			b="";
		}
		else
			b+=a;
	}
		if(b.equals("")){
			if(ret.containsKey(b)){
				 ret.put(b, ret.get(b)+1);
			}
		}
		else
			ret.put(b, 1);
	return ret;
}
}
