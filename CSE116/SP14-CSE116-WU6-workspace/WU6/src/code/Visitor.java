package code;

import code.LRStruct.IAlgo;

/**
 * A visitor for an LRStruct<String> that computes the number of  Strings
 * in an LRStruct<String> that begin with the lower case letter 'a'.  A 
 * String s begins with the lower case letter 'a' if and only if 
 *     s.charAt(0) == 'a'
 */
public class Visitor implements IAlgo<Integer, String, Object> {
	
	@Override
	public Integer emptyCase(LRStruct<String> host, Object arg) {
		return 0;
	}

	@Override
	public Integer nonEmptyCase(LRStruct<String> host, Object _) {
		int i =0;
		String c=host.getDatum();
		host.execute(this, _);
		if(c.charAt(0)=='a'){
			i++;
		}
		return i;
	}
}
