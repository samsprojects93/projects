package code;

import code.LRStruct.IAlgo;

/**
 * A visitor for an LRStruct<String> that computes the number of Strings
 * of a given length (len) in an LRStruct<String>.
 * 
 * For example, if the LRStruct myList is ("Fred" "Wilma" "Sue" "Betty") 
 * then:
 *   myList.execute(new StringLengthVisitor(), 3) is 1 -- "Sue"
 *   myList.execute(new StringLengthVisitor(), 4) is 1 -- "Fred"
 *   myList.execute(new StringLengthVisitor(), 5) is 2 -- "Wilma" and "Betty"
 */
public class StringLengthVisitor implements IAlgo<Integer, String, Integer> {
	
	@Override public Integer emptyCase(LRStruct<String> host, Integer len) {
		return 0;
	}

	@Override public Integer nonEmptyCase(LRStruct<String> host, Integer len) {
		String s = host.getDatum();;
		int i = s.length();
		int a =len;
		if(i==a){
			return 1+host.getRest().execute(this, len);
		}
		else 
			return host.getRest().execute(this, len);
	}
}
