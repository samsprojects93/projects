package tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import code.LRStruct;
import code.StringLengthVisitor;

public class Tests {

	private StringLengthVisitor v;
	private LRStruct<String> lst;
	private String LETTERS = "abcdefghijklmnopqrstuvwxyz";
	private Random random = new Random();

	private String feedback(LRStruct<String> lst, int len, int e, int a) {
		return "I executed your 'StringLengthVisitor' on this list, "+lst+", expecting the number of strings of length "+len+" to be "+e+", but the answer I got was "+a;
	}

	private String random(int minLength, int maxLength) {
		String s = "";
		int length = minLength + random.nextInt(maxLength-minLength+1);
		for (int i=0; i<length; i++) {
			int r = random.nextInt(LETTERS.length());
			s += LETTERS.charAt(r);
		}
		return s;
	}
	
	private int randomIntInRange(int minLength, int maxLength) {
		int length = minLength + random.nextInt(maxLength-minLength+1);
		return length;
	}
	
	private LRStruct<String> randomList(int nTarget, int targetLength, int nOthers
			) {
		ArrayList<String> list = new ArrayList<String>();
		for (int i=0; i<nTarget; i++) {
			list.add(random(targetLength,targetLength));
		}
		for (int i=0; i<Math.floor(nOthers/2.0); i++) {
			list.add(random(0,targetLength-1));
		}
		for (int i=0; i<Math.ceil(nOthers/2.0); i++) {
			list.add(random(targetLength+1,10));
		}
		Collections.shuffle(list);
		LRStruct<String> lrs = new LRStruct<String>();
		for (String s: list) {
			lrs.insertFront(s);
		}
		return lrs;
	}
	
	@Before public void setUp() {
		lst = new LRStruct<String>();		
		v = new StringLengthVisitor();
	}
	
	@Test public void testA() {
		int length = randomIntInRange(1,9);
		int expected = 0;
		int actual = lst.execute(v, length);
		assertTrue(feedback(lst,length,expected,actual), expected == actual);
	}
		
	@Test public void testB() {
		lst.insertFront("Fred").insertFront("Wilma").insertFront("Pebbles");
		int length = 4;
		int expected = 1;
		int actual = lst.execute(v, length);
		assertTrue(feedback(lst,length,expected,actual), expected == actual);
	}

	@Test public void testC() {
		lst.insertFront("Amy").insertFront("Bob").insertFront("Cloe");
		int length = 3;
		int expected = 2;
		int actual = lst.execute(v, length);
		assertTrue(feedback(lst,length,expected,actual), expected == actual);
	}

	@Test public void testD() {
		lst.insertFront("Samantha").insertFront("Robert").insertFront("Charlie");
		int length = 7;
		int expected = 1;
		int actual = lst.execute(v, length);
		assertTrue(feedback(lst,length,expected,actual), expected == actual);
	}

	
	@Test public void test00() { randomTest( 0); }
	@Test public void test01() { randomTest( 1); }
	@Test public void test02() { randomTest( 2); }
	@Test public void test03() { randomTest( 3); }
	@Test public void test04() { randomTest( 4); }
	@Test public void test05() { randomTest( 5); }
	@Test public void test06() { randomTest( 6); }
	@Test public void test07() { randomTest( 7); }
	@Test public void test08() { randomTest( 8); }
	@Test public void test09() { randomTest( 9); }
	@Test public void test10() { randomTest(10); }
	@Test public void test11() { randomTest(11); }
	@Test public void test12() { randomTest(12); }
	@Test public void test13() { randomTest(13); }
	@Test public void test14() { randomTest(14); }
	@Test public void test15() { randomTest(15); }
	@Test public void test16() { randomTest(16); }
	@Test public void test17() { randomTest(17); }
	@Test public void test18() { randomTest(18); }
	@Test public void test19() { randomTest(19); }
	@Test public void test20() { randomTest(20); }
	@Test public void test21() { randomTest(21); }

	public void randomTest(int expected) {
		int targetLength = randomIntInRange(1,9);
		lst = randomList(expected, targetLength, 100-expected);
		int actual = lst.execute(v, targetLength);
		assertTrue(feedback(lst,targetLength,expected,actual), expected == actual);
	}

}
