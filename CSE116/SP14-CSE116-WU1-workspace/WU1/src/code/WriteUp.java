package code;

public class WriteUp {

	public boolean solution (String s, char c){
		int i =0;
		while(i<s.length()){				
			if(s.charAt(i)== c && s.charAt(i+1)== c && s!=null && s!="")
				return true;
			i++;
		}
	 return false;
	}
}
