package code;

public class WriteUp<T> {
	WriteUp<T>[][]IN;
    public boolean [][] solution(T[][] IN){
    	boolean [][] OUT = new boolean [IN.length][IN[0].length];
	 for(int i = 0; i<IN.length;i++){
		 for(int j = 0;j<IN[0].length;j++){
			 if(IN[i][j]==null){
				 OUT[i][j]=true;
			 }
			 else
				 OUT[i][j]=false;
		 }
	 }
	 return OUT;
    }	
}
