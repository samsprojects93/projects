module prob548 (y, x, clock, reset);
output y;
input x,clock,reset;
reg [1:0] state;
parameter a = 2'b00, b = 'b01, c = 2'b10, d =2'b11;
always @ (posedge clock, negedge reset)
	if(~reset) state =a;
	else 
		case(state)
			a: if(x) state <= c; else state <=b;
			b: if(x) state <= d; else state <=c;
			c: if(x) state <= d; else state <=b;
			d: if(x) state <= a; else state <=c;
		endcase
	assign y = (state == d);
endmodule


module prob548_tb;
	wire y;
	reg x,clock,reset;
	prob548 t1(y,x,clock,reset);
	initial #200 $finish;
	initial begin
		$dumpfile("prob548.vcd");
		$dumpvars(0,prob548_tb);
		reset = 0;
		clock = 0;
		#5 reset = 1;
		repeat (16) #5 clock = ~clock;
	end
	initial begin 
		x = 0;
		#15 x = 1;
		repeat (8) #10 x = ~x;
	end
	initial
		$monitor("X = %b, Clock = %b, Reset = %b, Z = %b",x,clock,reset,y);
endmodule
