/*
module prob535(z,x,y,a,b,clock,rst);
input x,y,a,b,clock,rst;
output z;
*/
/*
reg A,B,n1,a1,a2,a3,a4,o1,o2;
always @ (posedge clock)
begin
	n1 <= ~y;
	a1 <= x && n1;
	a2 <= x && B;
	a3 <= x && A;
	a4 <= x && ~B;
	o1 <= a1 || a2;
	o2 <= a3 || a4;
	A <= o1;
	B <= o2;
assign	z <= A;
end
*/
/*
wire out1, out2, out3, out4,A,B;
and a2(out1, x, ~y);
and a3(out2, x, B);
and a4(out3, x, A);
and a5(out4, x, ~B);
or o6(A,out1,out2);
or o7(B,out3,out4);
assign z = A;

endmodule

///////////////////////////////////TB/////////////////////////////////

module prob535_tb;
reg  z,x,y,a,b,clock;
prob535 t1 (z,x,y,a,b,clock);
input x,y,a,b,clock;
output z;
initial fork
        begin 
	$dumpfile("prob535_text.vcd");
	$dumpvars(0,prob535_tb);
	$monitor("X = %b | Y = %b| Z = %b",x,y,z);
end
        begin
	if(a == 0)
	z<=0;
	else
	z<=1;
end
join


endmodule
*/
module prob535 (z,a,b,x,y,clock,rest);
output z,a,b;
input x,y,clock,rest;
wire ta,tb;
dff ff1(a,ta,clock,rest);
dff ff2(b,tb,clock,rest);
assign ta = (x && ~y) || (x && b);
assign tb = (x && a) || (x && ~b);
assign z = a && a;
endmodule


module dff(q,j,clock,rest);
	output reg q;
	input j,clock,rest;
	always @ (posedge clock, negedge rest)
		if(rest ==0) q <=0;
		else q <= j;
endmodule



module prob535_tb;
reg tx,ty,tclock,trest;
wire tz,ta,tb;
prob535 w1(tz,ta,tb,tx,ty,tclock,trest);
initial #200 $finish;
initial begin 
	$dumpfile("prob535.vcd");
	$dumpvars(0,prob535_tb);
	$monitor("Z : %b | A = %b | B = %b | X = %b | Y = %b | C = %b | R = %b ",tz,ta,tb,tx,ty,tclock,trest);
	end
initial begin
	tclock <= 1'b0;
	trest <= 1'b0;
	#5 trest <= 1'b1;
	forever #5 tclock = ~tclock;
	end
initial fork
	tx <= 1'b0;
	ty <= 1'b0;
	repeat (20) #15 tx <= ~tx;
	repeat (20) #10 ty <= ~ty;  
	join
endmodule	
