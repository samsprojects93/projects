#include <round.h>
#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>

#include "devices/input.h"
#include "devices/shutdown.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "threads/interrupt.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/pte.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "userprog/syscall.h"
#include "vm/frame.h"

static void syscall_handler (struct intr_frame* f);
static void halt (void);
static void exit (int exitStatus);
static pid_t exec (const char *ex);
static int wait (pid_t id);
static bool create (const char *f, unsigned size);
static bool remove (const char *f);
static int open (const char *f);
static int filesize (int i);
static int read (int i, void *buff, unsigned size);
static int write (int i, const void *buff, unsigned size);
static void seek (int i, unsigned position);
static unsigned tell (int i);
static void close (int i);
static mapid_t sys_mmap (int fd, void *addr);
static void sys_munmap (mapid_t mapid);
static struct user_file* findFile (int id)

void syscall_init (void) 
{
	intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void syscall_handler (struct intr_frame *f) 
{
	if (!is_user_vaddr(p) || !is_user_vaddr(p + strlen(p))) exit(-1);
	
	/* This is where we decipher our system calls and send them to the functions, I think f is input*/
}

static void halt (void) 
{
	shutdown_power_off();
}

static void exit (int exitStatus) 
{
	struct thread *cur = thread_current();
	cur->process_info->exit_status = exitStatus;
	thread_exit();
}

static pid_t exec (const char* ex) 
{
	pid_t id = (pid_t)process_execute(ex);

	if (!is_user_vaddr(ex) || !is_user_vaddr(ex + strlen(ex))) exit(-1);
	if (id == -1) return -1;

	struct thread* cur = thread_current();
	sema_down(thread_current()->process_info->sema_load);

	if (cur->process_info->child_load_success) return pid;
	else return -1;
}

static int wait (pid_t id) 
{
	return process_wait(id);
}

static bool create (const char *f, unsigned size) 
{
	if (!is_user_vaddr(f) || !is_user_vaddr(f + strlen(f))) exit(-1);

	lock_acquire (&file_lock);
	bool res = filesys_create(f, size);
	lock_release (&file_lock);

	return res;
}

static bool remove (const char *f) 
{
	if (!is_user_vaddr(f) || !is_user_vaddr(f + strlen(f))) exit(-1);

	lock_acquire (&file_lock);
	bool res = filesys_remove(f);
	lock_release (&file_lock);
	return res;
}

static int open (const char* f) 
{
	if (!is_user_vaddr(f) || !is_user_vaddr(f + strlen(f))) exit(-1);
	struct thread* cur = thread_current();

	lock_acquire (&file_lock);
	struct file* fileOpen = filesys_open(f);
	lock_release (&file_lock);

	if (fileOpen == NULL) return -1;

	struct file_info* fileInfo;
	fileInfo = (struct file_info*)malloc(sizeof(struct file_info));

	f_info->pos = 0;
	f_info->p_file = fileOpen;

	int i;

	for(i = 0; i < 128; i++)
	{
		if (cur->array_files[i] == NULL)
		{
			cur->array_files[i] = fileInfo;
			break;
		}
	}

	return i;
}

static int fileSize (int i) 
{
	if (findFile(i) == NULL) exit(-1);
	struct thread* cur = thread_current();

	lock_acquire (&file_lock);
	int res = (int)file_length(cur->array_files[i]->p_file);
	lock_release (&file_lock);
	return res;
}

/* I feel like we should change this function, everybody does it differentely */
static int read (int i, void *buff, unsigned size) 
{
	if ((!is_user_vaddr(f) || !is_user_vaddr(f + strlen(f)) || 
	    (findFile(i) == NULL) && i != STDOUT_FILENO) 
	    exit(-1);
	struct thread* cur = thread_current();

	if (i == STDIN_FILENO)
	{
		int res = 0;
		for (unsigned i = 0; i < size; i++)
		{
			*(uint8_t *)(buff + i) = input_getc();
			res++;
			buff++;
		}
		
		return res;
	}

	else
	{
		struct file* pf = cur->array_files[i]->p_file;
		unsigned file_offset = cur->array_files[i]->pos;

		lock_acquire (&file_lock);
		int res = file_read_at (pf, buff, size, file_offset);
		cur->array_files[i]->pos = cur->array_files[i]->pos + res;
		lock_release (&file_lock);
		return res;
	}
}


static int write (int i, const void *buff, unsigned size) 
{
	if ((!is_user_vaddr(f) || !is_user_vaddr(f + strlen(f)) || 
	    (findFile(i) == NULL) && i != STDOUT_FILENO) 
	    exit(-1);
	struct thread* cur = thread_current();

	if (i == STDOUT_FILENO)
	{
		putbuf(buff, size); /* I don't know what this does */
		return size;
	}

	else
	{
		lock_acquire(&file_lock);
		int res = file_write_at(cur->array_files[i]->p_file, buff, size, cur->array_files[i]->pos);
		cur->array_files[i]->pos = cur->array_files[i]->pos + res;
		lock_release(&file_lock);
		return res;
	}
}

static void seek (int i, unsigned pos) 
{
	if (findFile(i) == NULL) exit(-1);
	struct thread* cur = thread_current();

	if (pos > (unsigned)_filesize(i)) cur->array_files[i]->pos = (unsigned)_filesize(i);
	else cur->array_files[i]->pos = pos;
}

/* Return  current file's position*/
static unsigned tell (int i) 
{
	if (findFile(i) == NULL) exit(-1);
	struct thread* cur = thread_current();
	return cur->array_files[i]->pos;
}

/* Close a file */
static void close (int i) 
{	
	if (findFile(i) == NULL) exit(-1);
	struct thread* cur = thread_current();

	lock_acquire (&file_lock);

	struct file* curFile = cur->array_files[i]->p_file;
	free(cur->array_files[i]);
	cur->array_files[i] = NULL;
	file_close(curFile);

	lock_release (&file_lock);
}

static mapid_t mmap (int i, void *addr) 
{
	/* Shen can do this <3 */
	struct thread *curr = thread_current();
	if(!is_user_vaddr(addr) 
	|| pg_ofs (addr)!=0 
	|| addr == 0
	|| fileSize(i) ==0
	|| i == STDIN_FILENO 
	|| i == STDOUT_FILENO )
	{
		return MAP_FAILED;
	}
	
	void *add = NULL;
	int fsize = fileSize(i);
	for(add = addr; add < addr + fsize; add += PGSIZE)
	{
		uint32_t *pt = sup_pt_pte_lookup (curr->pagedir,add,false);
		if(pte != NULL)
		{
			if(sup_pt_pte_lookup(pte) != NULL)
			{
				return MAP_FAILED;
			}
		}
	}
	
	mapid_t Tempid = allocate_mapid();
	struct map_struct *m = (struct map_struct*)malloc(sizeof (struct mmap_struct));
	if(m = NULL)
	{
		return MAP_FAILED;
	}
	
	m->mapid = Tempid;
	m->vaddr = addr;
	lock_acquire (&glb_lock_filesys);
	struct file* new_file_ref = file_reopen (curr->array_files[i]->p_file);	
	lock_release (&glb_lock_filesys);
	if (new_file_ref == NULL)
    	{
      		free (m);
      		return MAP_FAILED;
   	}
 	m->p_file = new_file_ref;
	list_push_back (&curr->mmap_list, &m->elem);
	uint32_t read_bytes = f_size;
	uint32_t zero_bytes = ROUND_UP (read_bytes, PGSIZE) - read_bytes;
	void* Tpage = addr;
	block_sector_t sector_idx = byte_to_sector (file_get_inode (m->p_file), 0);
	while (read_bytes > 0 || zero_bytes > 0)
	{
		size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
      		size_t page_zero_bytes = PGSIZE - page_read_bytes;
		uint32_t flag = (page_read_bytes > 0 ? 0 : FS_ZERO) | POS_DISK | TYPE_MMFile;
		mark_page (upage, NULL, page_read_bytes, flag, sector_idx);
		read_bytes = read_bytes - page_read_bytes;
      		zero_bytes = zero_bytes - page_zero_bytes;
      		Tpage += PGSIZE;
      		sector_idx = sector_idx + PGSIZE / BLOCK_SECTOR_SIZE
	}
	return Tempid;
	/*done*/
}

static void munmap (mapid_t mapping) 
{
	/* Shen can do this <3 */
	struct thread *curr = thread_current ();
	struct list_elem *Telem = NULL;
  	struct list_elem *next_elem = NULL;
  	uint32_t f_size = 0;
	for (e = list_begin (&curr->mmap_list); Telem != list_end (&curr->mmap_list); Telem = next_elem)
	{
		struct mmap_struct* m = list_entry (Telem, struct mmap_struct, elem);
		if(m->mapid == mapping)
		{
			f_size = file_length (m->p_file);
          		uint32_t write_bytes = f_size;
          		void* upage = m->vaddr;
			while(write_bytes >0)
			{
				size_t page_write_bytes = write_bytes < PGSIZE ? write_bytes : PGSIZE;
				uint32_t* pte = sup_pt_pte_lookup (curr->pagedir, upage, false);
              			struct page_struct* ps = sup_pt_ps_lookup (pte);
				if (sup_pt_fs_is_dirty (ps->fs))
					file_write_at (m->p_file, upage, write_bytes, upage - m->vaddr);
				uint32_t tmp_pte_content = *pte;
				if (sup_pt_delete (pte))
					if ((tmp_pte_content & PTE_P) != 0)palloc_free_page (pte_get_page (tmp_pte_content));
				write_bytes = write_bytes - page_write_bytes;
              			upage = upage + PGSIZE;
				file_close (m->p_file);
				list_remove (Telem);
				free (m);
				break;
			}

		}
		else
			next_elem = list_next(Telem);
	}
	/*done*/
}

/* allocate a new mmap file id */
static mapid_t allocate_mapid (void)
{
	/* Shen can do this <3 */
  struct thread *curr = thread_current ();

  mapid_t re = curr->next_mapid;
  curr->next_mapid++;

  return re;
	/*done*/
}

/* New function used to find our file we have an id for and return a pointer to it */
static struct user_file* findFile (int id)
{
	if(id == NULL) return NULL;
	
	struct thread *cur = thread_current();
	struct list_elem *e = list_begin (&cur->files);
	struct user_file *f = NULL;

	while (e != list_end(&cur->files))
	{
		f = list_entry(e, struct user_file, thread_elem);
		if (f->fid == id) return f;
		e = list_next(e);
	}

	return NULL;
}
