#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "devices/shutdown.h"
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "filesys/file.h"
#include "userprog/process.h"

#include <string.h>

static void syscall_handler (struct intr_frame *);

static int get_user (const uint8_t *uaddr);

static void halt (void);
static void exit (int status);
static pid_t exec (const char *cmd_line);
static int wait (pid_t pid);
static bool create (const char *file, unsigned initial_size);
static bool remove (const char *file);
static int open (const char *file);
static int filesize (int fd);
static int read (int fd, void *buffer, unsigned size);
static int write (int fd, const void *buffer, unsigned size);
static void seek (int fd, unsigned position);
static unsigned tell (int fd);
static void close (int fd);
//////////////////////////////////////////////////////

static void set_arg (char *arg);

//////////////////////////////////////////////////////

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");

  /* Initialize system call lock. */
  lock_init (&call_lock);
}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  /* Get syscall number. */
  int signal = get_user(f->esp);
  void *arg[3];

  //printf ("system call number: %d\n", signal);

  switch (signal)
  {
    case SYS_HALT:
      halt ();
      break;
    case SYS_EXIT:
      exit ((int) arg[0]);
      break;
    case SYS_EXEC:
      exec ((const char *) arg[0]);
      break;
    case SYS_WAIT:
      wait ((int) arg[0]);
      break;
    case SYS_CREATE:
      create ((const char *) arg[0], (unsigned) arg[0]);
      break;
    case SYS_REMOVE:
      remove ((const char *) arg[0]);
      break;
    case SYS_OPEN:
      open ((const char *) arg[0]);
      break;
    case SYS_FILESIZE:
      filesize ((int) arg[0]);
      break;
    case SYS_READ:
      read ((int) arg[0], arg[1], (unsigned) arg[2]);
      break;
    case SYS_WRITE:
      write ((int) arg[0], arg[1], (unsigned) arg[2]);
      break;
    case SYS_SEEK:
      seek ((int) arg[0], (unsigned) arg[1]);
      break;
    case SYS_TELL:
      tell ((int) arg[0]);
      break;
    case SYS_CLOSE:
      close ((int) arg[0]);
      break;
    default:
      exit (-1);
      break;
  }

  printf ("system call!\n");
  thread_exit ();
}

static void
halt (void)
{
  shutdown_power_off ();
}

static void
exit (int status)
{
  thread_current ()->exit_status = status;
  process_exit ();
  thread_exit ();
}

static pid_t
exec (const char *cmd_line)
{
  process_execute (cmd_line);
  return 0;
}

static int
wait (pid_t pid)
{
  return pid;
}

static bool
create (const char *file, unsigned initial_size)
{
  file++;
  return initial_size;
}

static bool
remove (const char *file)
{
  file++;
  return true;
}

static int
open (const char *file)
{
  file++;
  return 0;
}

static int
filesize (int fd)
{
  return fd;
}

static int
read (int fd, void *buffer, unsigned size)
{
/*
  
  buffer++;
  size++;
  return fd;
*/

  
}

static int
write (int fd, const void *buffer, unsigned size)
{
  /* Acquire lock before writing. */
  lock_acquire (&call_lock);
  printf ("fd: %d\n", fd);
  if (fd == 1)
  {
    putbuf (buffer, size);
    lock_release (&call_lock);
    return size;
  }
  /*
  else
  {
    struct file *file = thread_current ()->file_list[fd];
    lock_release (&call_lock);
    return file_write (file, buffer, size);
  }
  */
  return fd;
}

static void
seek (int fd, unsigned position)
{
  fd++;
  position++;
}

static unsigned
tell (int fd)
{
  return fd;
}

static void
close (int fd)
{
  fd++;
}
/* Reads a byte at user virtual address UADDR.
   UADDR must be below PHYS_BASE.
   Returns the byte value if successful, -1 if a segfault
   occurred. */
static int
get_user (const uint8_t *uaddr)
{
  int result;
  asm ("movl $1f, %0; movzbl %1, %0; 1:"
       : "=&a" (result) : "m" (*uaddr));
  return result;
}

//////////////////////////////////////////////////////
/* Use to read the thread stack */
static void set_arg (char *arg)
{
  int sd = get_user(thread_current()->stack);
  /*Make arg into a string */
  arg[0] = (char)sd;                         
  arg[1] = '\0';             
  /*Start seting the arg */       
  for(int i=1;(char)sd+i != '\0';i++)
  {
   strcat (arg, (const char*)sd+i);
  }
  
}
////////////////////////////////////////////////////////