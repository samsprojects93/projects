#include <vector>
using namespace std;
class A2
{
private:
int rowNum;
int columnNum;

public: 
vector<vector<double>> vec;
A2():rowNum(0), columnNum(0){}

A2(int r ,int c):rowNum(r), columnNum(c)
{
if(columnNum > 0 && rowNum > 0)
{
vector<double> temp;
for(int i=0;i<columnNum; i++)
temp.push_back(0.0);

vec.push_back(temp);

for(int i=1;i<rowNum; i++)
vec.push_back(temp);
}
}
//used for passing const reference
const double& A2::operator()(int nCol, int nRow) const
{
return vec[nCol][nRow];
}

//used for regular reference
double& A2::operator()(int nCol, int nRow)
{
return vec[nCol][nRow];
}

double A2::rsum(int r)
{
double sum = 0.0;

for(int i=0;i<columnNum; i++)
{
sum+=vec[r][i];
}
return sum;
}

double A2::csum(int c)
{
double sum = 0.0;
for(int i=0;i<rowNum; i++)
{
sum+=vec[i][c];
}
return sum;
}
};
