#ifndef A6_HPP
#define A6_HPP

#include "token.hpp"
#include <stack>
#include <stdexcept>

using namespace std;

template<typename Iter> double RPN(Iter first, Iter last){
stack<double> myStack;
//int counter = -1;
/*
auto Tfirst = first;
auto Tlast = last;
if(Tfirst == 0 && Tlast == 0 ) throw std::runtime_error("error");
if(Tfirst == Tlast) throw std::runtime_error("error");
if(Tfirst.is_operand() != true) throw std::runtime_error("error");
if(1+Tfirst.is_operand() != true) throw std::runtime_error("error");
if(Tlast.is_operator() != true) throw std::runtime_error("error");
*/
for(auto it = first;it != last;++it){
//counter++;
auto value =*(it);

//check format
/*
if(counter != 0){
if(counter%2 == 0 && !value.is_operator())
throw std::runtime_error("error");
if(counter%2 == 1 && !value.is_operand())
throw std::runtime_error("error");
}
*/


if(value.is_operand()){
myStack.push(value.as_operand());
}

if(value.is_operator()){
double firstNum,secondNum;
if(myStack.size() == 0) throw std::runtime_error("error");
secondNum = myStack.top();
myStack.pop();
if(myStack.size() == 0) throw std::runtime_error("error");
firstNum = myStack.top();
myStack.pop();
switch(value.as_operator()){
case'+':
myStack.push(firstNum + secondNum);
break;
case'-':
myStack.push(firstNum - secondNum);
break;

case'*':
myStack.push(firstNum * secondNum);
break;

case'/':
if(secondNum == 0.0)
throw std::runtime_error("error");
myStack.push(firstNum / secondNum);
break;
}

}
}
if(myStack.size() != 1) throw std::runtime_error("error");
return myStack.top();
}
#endif
