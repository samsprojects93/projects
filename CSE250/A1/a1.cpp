#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <set>
#include <algorithm>

using namespace std;

int main()
{
ifstream file ("foo.txt");

if (!file.is_open())
{
cout << "Unable to open file"<<endl; 
return 0;
}

vector<string> dna_seq;
int k = 0;
int m = 0;
file>>k;
file>>m;

if(k < 3 || m < 1 || k > 10 || m == 1 )
{
cout<<"Error"<<endl;
return 0;
}
for (int i = 0; i < m; i++)
{
string input;
file>>input;
int size = input.length();
if(size == k)
{
cout<<"Error"<<endl;
return 0;
}

for (size_t j = 0; j <= input.length() - k; j++)
{
dna_seq.push_back(input.substr(j, k));
}

cout<<"For Input: " + input<<endl;

sort(dna_seq.begin(), dna_seq.end());
int dnasize = dna_seq.size();
for (int i = 0; i < dnasize; i++)
{
int counter = 1;
if(i == dnasize - 1)
cout<<dna_seq[i]<<" "<<counter<<endl;
else
for (int j = i + 1; j < dnasize ; j++)
{
if(dna_seq[i] == dna_seq[j])
counter++;
else
{
cout<<dna_seq[i]<<" "<<counter<<endl;
i=j - 1;
break;
}
}
}
}

if(!file.eof())
{
system("cln");
file.close();
cout<<"Error"<<endl;
return 0;
}

file.close();

system("pause");
return 0;
}
