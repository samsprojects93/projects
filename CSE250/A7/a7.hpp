#ifndef A7_HPP
#define A7_HPP

#include "symbol.hpp"
#include<queue>
#include<vector>
class compare{
public:
bool operator()(const bnode<symbol>* left, const bnode<symbol>* right){
return ((right->value < left->value));
}
};
// IMPLEMENT YOUR FUNCTION huffman_tree
template <typename Iter>
bnode<symbol>* huffman_tree(Iter first, Iter last){
std::priority_queue<bnode<symbol>* , std::vector<bnode<symbol>* >, compare > PTree;

for(;first != last;first++){
bnode<symbol>* leaf = new bnode<symbol>;
leaf->value = *first;
PTree.push(leaf);
}

while(PTree.size() != 1){
bnode<symbol>* temp = new bnode<symbol>;
temp ->left = PTree.top();
PTree.pop();
temp ->right = PTree.top();
PTree.pop();
temp ->value = symbol(0,temp->left->value.count + temp->right->value.count);
PTree.push(temp);
}
return PTree.top();
};

// IMPLEMENT YOUR FUNCTION release_tree
void release_tree(bnode<symbol>* root){
if(root != 0){
release_tree(root->left);
release_tree(root->right);
delete root;
//root = NULL;
}
};

#endif // A7_HPP
