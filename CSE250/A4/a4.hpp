#ifndef A4_HPP
#define A4_HPP

#include "Maze.hpp"
#include <queue>
#include <iostream>
//#include <tuple>
//#include <queue>
#include <vector>

using namespace std;

typedef pair<int, int> Pair;

// implement your function distance
int distance(Maze& maze, int sx, int sy, int fx, int fy) {
Pair beg(sx, sy);
Pair end(fx, fy);
int N = maze.ncol();
int M = maze.nrow();

// stores distance from the begining, negative for unvisited
int** dist = new int*[N];
for (int i = 0; i < N; i++)
dist[i] = new int[M];

 // can move in four directions
vector<Pair> dmove;
dmove.push_back(Pair(0, -1));
dmove.push_back(Pair(0, +1));
dmove.push_back(Pair(-1, 0));
dmove.push_back(Pair(+1, 0));

queue<Pair> q;
 
// start from the begining point
q.push(beg);
dist[beg.first][beg.second] = 0;
while (! q.empty()) 
{
// p is current position
Pair p = q.front();
q.pop();
 
// if this is the ending position, finish.
if (p.first == end.first && p.second == end.second)
break;
 
// find adjacent unvisited positions
for (vector<Pair>::iterator it = dmove.begin(); it != dmove.end(); ++it) 
{
int y = p.first + it->first;
int x = p.second + it->second;
// ensure the new position is unvisited and visitable
if (x >= 0 && x < M && y >= 0 && y < N && !maze.is_visited(y, x)
&& dist[y][x] < 0) 
{
// put the unvisited position in the queue
q.push(Pair(y, x));
         
// update the distance at the unvisited position
dist[y][x] = dist[p.first][p.second] + 1;
}
}
}

int result = dist[end.first][end.second];

//free 2D array memory
for (int i = 0; i < N; i++)
delete [] dist[i];
delete [] dist;

return result;
} // distance

#endif // A4_HPP
