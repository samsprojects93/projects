<workforce>
{	
	for $i in fn:distinct-values((/projects/project/leader/name, /projects/project/analyst/name))
	let $j := if((some $ti in /projects/project/leader/name satisfies data($ti)=data($ti))
		  and (some $tj in /projects/project/analyst/name satisfies data($tj)=data($tj)))
		  then "leader,analyst"
		  else if (some $t in /projects/project/leader/name satisfies data($i)=data($t))
		  then "leader"
		  else "analyst"
	order by $j
	return <emp roles="{$j}" name="{data($i)}">
	<projects>
	{
		for $q in /projects/project
		return if (($q/leader/name=$i) or ($q/analyst/name=$i))
		then <proj>{data($q/@prid)}</proj>
		else()
	}
	</projects>
	</emp>
}
</workforce>


