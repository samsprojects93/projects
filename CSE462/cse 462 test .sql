CREATE TABLE Book
(
Title VARCHAR(30) PRIMARY KEY,
Price INT NOT NULL,
Year INT NOT NULL
);

INSERT INTO Book VALUES ('DB Systems: the complete book', 112, 
2008);
INSERT INTO Book VALUES ('Active DBs', 78, 2008);
INSERT INTO Book VALUES ('The Art of CP', 339, 1997);
INSERT INTO Book VALUES ('Concrete Mathematics', 130, 1990);
INSERT INTO Book VALUES ('GraphBase', 97, 1990);

CREATE TABLE Author
(
Name VARCHAR(30) NOT NULL,
Booktitle VARCHAR(30) NOT NULL,
Position INT NOT NULL,
FOREIGN KEY (Booktitle) REFERENCES Book(Title),
PRIMARY KEY (Booktitle, Position)
);

INSERT INTO Author VALUES ('Garcia-Molina', 'DB Systems: the complete book', 
1);
INSERT INTO Author VALUES ('Ullman', 'DB Systems: the complete book', 2);
INSERT INTO Author VALUES ('Widom', 'DB Systems: the complete book', 3);
INSERT INTO Author VALUES ('Widom', 'Active DBs', 1);
INSERT INTO Author VALUES ('Ceri', 'Active DBs', 2);
INSERT INTO Author VALUES ('Knuth', 'The Art of CP', 1);
INSERT INTO Author VALUES ('Graham', 'Concrete Mathematics', 1);
INSERT INTO Author VALUES ('Knuth', 'Concrete Mathematics', 2);
INSERT INTO Author VALUES ('Patashnik', 'Concrete Mathematics', 3);
INSERT INTO Author VALUES ('Knuth', 'GraphBase', 1);


---------------------------Problem 1 -----------------------------------------------
/*find the average book price for each author */
SELECT A1.Name,AVG(B1.Price) AS Averageprice
FROM Author A1,Book B1
WHERE B1.Title = A1.Booktitle
GROUP BY A1.Name;
------------------------------------------------------------------------------
Select A.Name,AVG(B.Price) AS average
from Author A join Book B on (B.Title = A.Booktitle)
group by A.Name;

-------------------------Problem 2----------------------------------------------
/*For each author, return the maximum number N of books published in a single Year. Sort tuples 
in descending order of N */
create view NumBook (Name,Year,Num)as 
select A1.Name , B1.Year , Count(*)
from Author A1, Book B1
where A1.Booktitle = B1.Title
Group by A1.Name,B1.Year;

select N1.Name,N1.Num
from NumBook N1
where not exists
	(
	select 3
	from NumBook N2
	where N2.Num > N1.Num And N2.Name = N1.Name 
	)
group by N1.Name,N1.Num
order by N1.Num desc;

----------------------------------------------------or------------
select N1.Name,Max(N1.Num)as maxnum
from NumBook N1
group by N1.Name
order by maxnum desc;
-----------------------------------or---------------------

select*
from NumBook N1
Minus
select N1.Name,N1.Year,N1.Num
from NumBook N1 join NumBook N2 on (N1.Name = N2.Name And N1.Num < N2.Num)
order by 3 ;


---------------------------Problem 3--------------------------
/*List every author who was in the second or third position in all of his/her books*/

select A.Name,A.Position 
from Author A
where not exists
	(
	select 1
	from Author B
	where B.Name = A.Name And B.Position <>2 And B.Position <>3 
	)
group by A.Name,A.Position;
---------------------------------------or-------------------------------------
select A.Name
from Author A
Minus
select B.Name 
from Author B
where B.Position <>2 And B.Position <> 3;


---------------------------------Problem 4 -------------------------------------
/*Return all the authors whose average book price is 30% more than the average book price
of every other author */

create view AvggBook (Name,Price)as
select A.Name,AVG(B.Price)
from Book B Join  Author A on (B.Title = A.Booktitle)
group by A.Name;

select Name 
from AvggBook Av
where Av.Price > all
	(
	select 1.3*Av1.Price
	from AvggBook Av1
	where Av1.Price <> Av.Price
	);
--------------------------or ------------------------------------
select v.Name
from AvggBook v 
where v.Price > (select 1.3*Max(v1.Price)
		from AvggBook v1
		where v1.Name <> v.Name
		);
		
------------------------or -------------------------------------

select v.Name
from AvggBook v
where not exists
	(
	select *
	from AvggBook v1
	where v1.Name <> v.Name and v.Price <= (1.3*v1.Price) 
	);

	

CREATE TABLE Ranges
(
low INT NOT NULL,
high INT NOT NULL
);
INSERT INTO Ranges VALUES (71,90);
INSERT INTO Ranges VALUES (91,110);
INSERT INTO Ranges VALUES (111,130);
INSERT INTO Ranges VALUES (131,150);
INSERT INTO Ranges VALUES (151,500);

-------------------------Problem 5 ---------------------------------
/*Computer an histogram of book's prices (i.e.count how many books fall into each price range)*/ 
select R.low,R.High,B.Title
from Ranges R ,Book B
where B.Price >= R.low or B.Price <= R.High 
group by R.low, R.High,B.Title;


-------------------------Problem  6-------------------------------------
/*List all authors that in some year contributed to every book published that yesr */
select Distinct A.Name,B.Year
from Auther A,Book B
Minus
select Name, Year
from 
	(select Distinct A.Name, B.Title, B.Year 
	 from Author A, Book B
	 Minus
	 select A.Name,B.Title,B.Year 
	 from Author A Join Book B on A.Booktitle = B.Title
	);
--------------------------------------  Test   2 ---------------------------------

CREATE TABLE MOVIES (
 Title VARCHAR(30) NOT NULL,
 Year INT NOT NULL,
 Duration INT NOT NULL,
 MovieID INT PRIMARY KEY CHECK (MovieID > 0)
);
INSERT INTO MOVIES values ('Rambo', 1982, 93, 1);
INSERT INTO MOVIES values ('Rambo II', 1985, 96, 2);
INSERT INTO MOVIES values ('Rambo III', 1988, 101, 3);
INSERT INTO MOVIES values ('Terminator', 1984, 107, 4);
INSERT INTO MOVIES values ('Die Hard', 1988, 132, 5);
INSERT INTO MOVIES values ('The Expendables', 2010, 103, 6);

CREATE TABLE CAST (
!MovieId INT NOT NULL,
 Name VARCHAR(30) NOT NULL,
 PRIMARY KEY (MovieId,Name)
);
INSERT INTO CAST values (1, 'Stallone');
INSERT INTO CAST values (2, 'Stallone');
INSERT INTO CAST values (3, 'Stallone');
INSERT INTO CAST values (4, 'Schwarznegger');
INSERT INTO CAST values (5, 'Willis');
INSERT INTO CAST values (6, 'Stallone');
INSERT INTO CAST values (6, 'Schwarznegger');
INSERT INTO CAST values (6, 'Willis');

-----------------------------------------Example #1 ----------------------

select M1.Title,M2.Title 
from Movies M1, Movies M2 
where M1.Year < M2.Year;

select *
from Movies;
---------------------------------------------Done --------------------------

----------------------------------------P Quest 1------------------------------
--List all the actors that only played in "Star War" and "The Return of Jedi"


select C.Actor
from Casts C, Casts C1
where C.Title ='Star War' 
And C1.Title ='Return of Jedi'
And C1.Actor = C.Actor
And not exists 
	(
	select *
	from Casts C2
	where C2.Actor = C.Actor 
	And C2.Title <>'Star War' 
	And C2.Title <> 'Return of Jedi'
	)
group by C.Actor;


-----------------------------------------------------------------------------