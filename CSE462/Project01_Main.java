package edu.buffalo.cse462;

import oracle.jdbc.pool.OracleDataSource;
import java.sql.*;
import java.io.*;

public class Project01_Main 
{

public static void main(String[] args) 
{

String username = args[0];
String passwd = args[1];

Boolean QY = true;
if(args[2].equals("RANGE_QUERY"))
QY = true;
else if(args[2].equals("NN_QUERY"))
QY = false;
else
System.out.println("|""+args[2]+"|"Error");



String argStr0 = args[3];
String argStr1 = args[4];

try 
{
Class.forName("oracle.jdbc.driver.OracleDriver");
String url = "jdbc:oracle:thin:" + username + "/" + passwd +
"@aos.acsu.buffalo.edu:1521/aos.buffalo.edu";
Connection conn = DriverManager.getConnection(url);

//Range queries (Given the name of a city c and a radius r (expressed in kilometer) return all the cities whose distance from c is strictly less than r. By distance we mean the length of the shortest trajectory over the earth's surface that conmects the two cities )


if(QY == true) 
{
Statement stmt = conn.createStatement();
String sql = "CREATE OR REPLACE VIEW CityDist(city,length) AS SELECT M1.City, 12742*ATAN2(SQRT((POWER(SIN(((M.Latitude*(3.14159265358979)/180)-(M1.Latitude*(3.14159265358979)/180))/2),2)+COS((M.Latitude*(3.14159265358979)/180))*COS((M1.Latitude*(3.14159265358979)/180))*POWER(SIN(((M.Longitude*(3.14159265358979)/180)-(M1.Longitude*(3.14159265358979)/180))/2),2))),SQRT(1-(POWER(SIN(((M.Latitude*(3.14159265358979)/180)-(M1.Latitude*(3.14159265358979)/180))/2),2)+COS((M.Latitude*(3.14159265358979)/180))*COS((M1.Latitude*(3.14159265358979)/180))*POWER(SIN(((M.Longitude*(3.14159265358979)/180)-(M1.Longitude*(3.14159265358979)/180))/2),2)))) FROM Map M, Map M1 WHERE M.City <> M1.City AND M.City = '"+argStr0+"'";
stmt.executeUpdate(sql);

Statement stmt1 = conn.createStatement();
String sql1 = "SELECT C.city, ROUND(length,2) FROM CityDist C WHERE C.length<"+argStr1+" ORDER BY C.length";
ResultSet RS = stmt1.executeQuery(sql1);


while(RS.next())
{
String col0 = RS.getString("city");
String col1 = String.valueOf(rs.getFloat("ROUND(length,2)"));
System.out.println(col0 +"|"+ col1);
}
RS.close();
stmt.close();
stmt1.close();

Statement closeview = conn.createStatement();
String closesq = "DROP VIEW CityDist";
closeview.executeUpdate(closesq);
closeview.close();
}

//Top-k nearest neighbors (Given the name of a city c and a strictly positive integer k return the k cities that are closest to c)

else
{
Statement stmt = conn.createStatement();
String sql = "CREATE OR REPLACE VIEW CityDist(city,length) AS SELECT M1.City, 12742*ATAN2(SQRT((POWER(SIN(((M.Latitude*(3.14159265358979)/180)-(M1.Latitude*(3.14159265358979)/180))/2),2)+COS((M.Latitude*(3.14159265358979)/180))*COS((M1.Latitude*(3.14159265358979)/180))*POWER(SIN(((M.Longitude*(3.14159265358979)/180)-(M1.Longitude*(3.14159265358979)/180))/2),2))),SQRT(1-(POWER(SIN(((M.Latitude*(3.14159265358979)/180)-(M1.Latitude*(3.14159265358979)/180))/2),2)+COS((M.Latitude*(3.14159265358979)/180))*COS((M1.Latitude*(3.14159265358979)/180))*POWER(SIN(((M.Longitude*(3.14159265358979)/180)-(M1.Longitude*(3.14159265358979)/180))/2),2)))) FROM Map M, Map M1 WHERE M.City <> M1.City AND M.City = '"+argStr1+"'";
stmt.executeUpdate(sql);

Statement stmtp = conn.createStatement();
String sqlp = "CREATE OR REPLACE VIEW CityDistSort(city,length) AS SELECT C.city, C.length FROM CityDist C ORDER BY C.length";
stmtp.executeUpdate(sqlp);
Statement stmt1 = conn.createStatement();
String sql1 = "SELECT CS.city, ROUND(length,2) FROM CityDistSort CS WHERE ROWNUM <= "+argStr2+" ORDER BY CS.length";
ResultSet RS = stmt2.executeQuery(sql2);

while(RS.next()) 
{
String col0 = rs.getString("city");
String col1 = String.valueOf(rs.getFloat("ROUND(length,2)"));
System.out.println(col0 + "|" + col1);
}
RS.close();
stmt.close();
stmt1.close();
stmtp.close();

Statement closeview = conn.createStatement();
String closesql = "DROP VIEW CityDist";
closeview.executeUpdate(closesql);
closeview.close();

Statement closeview1 = conn.createStatement();
String closesql1 = "DROP VIEW CityDistSort";
closeview1.executeUpdate(closesql1);
closeview1.close();
}
catch (ClassNotFoundException e) 
{
System.out.println("Could not load the JDBC driverasdf");
e.printStackTrace();
System.exit(0);
}
catch (SQLException e1) 
{
System.out.println("Could not connect to Oracleasdf");
e1.printStackTrace();
System.exit(0);
}
}
}
